﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ParandaIsikukoodid
{
    static class ParandaIsikukoodid
    {
        static Random rnd = new Random();

        static void Main(string[] args)
        {
            Dictionary<string, string> Isikukoodid = new Dictionary<string, string>();
     
        }
            static string GenereeriIsikukood(string isikukood)
        {
            if (isikukood.Check()) return isikukood;
            else
            {
                string algus = isikukood.Substring(0, 3);
                int päevAastas = (rnd.Next() % 365);
                string kuu = (päevAastas % 12 + 1).ToString("00");
                string päev = (päevAastas % 31 + 1).ToString("00");
                DateTime.TryParse(ToDatetime(algus + kuu + päev).ToShortDateString(), out DateTime sünnikuupäev);
                for (int kj = 0; kj < 10; kj++)
                {
                    isikukood = algus + kuu + päev + (rnd.Next() % 100).ToString("000") + kj.ToString();
                    if (isikukood.Check()) break;
                }
            }
            return isikukood;
        }
        static bool Check(this string isikukood)
        {
            int i = 0;

            int kj =
            isikukood.Substring(0, 10)
                .Select(x => x - '0')
                .ToArray()
                .Select(x => x * ((i++ % 9) + 1))
                .Sum()
                % 11
                ;
            if (kj == 10)
            {
                i = 2;
                kj = isikukood.Substring(0, 10).ToArray()
                    .Select(x => x * ((i++ % 9) + 1))
                .Sum() % 11 % 10;
            }

            return kj == isikukood[10] - '0';
        }
        static DateTime ToDatetime(this string isikukood)
        {
            DateTime x;

            DateTime.TryParse(
                (((isikukood[0] - '0' - 1) / 2 + 18).ToString() +
                    isikukood.Substring(1, 2) + "-" +
                    isikukood.Substring(3, 2) + "-" +
                    isikukood.Substring(5, 2))
                    , out x)
                    ;
            return x;
        }
    }
}
