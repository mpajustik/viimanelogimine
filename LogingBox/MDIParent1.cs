﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogingBox
{
    public partial class MDIParent1 : Form
    {
        private int childFormNumber = 0;

        public MDIParent1()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDIParent1_Load(object sender, EventArgs e)
        {
            Login();
        }

        private void AruandedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.kasutaja.Isikukood == null)
            {
                Login();
                AruandedToolStripMenuItem_Click(sender, e);
            }
            else
            {
                AruanneteSisestamine aruanneMDIChild = new AruanneteSisestamine();
                aruanneMDIChild.MdiParent = this;
                aruanneMDIChild.Show();
            }
        }

        private void LoginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var f in this.MdiChildren) f.Close();
            Login();
        }
        private void Login()
        {
            DialogResult tulemus;
            using (var loginForm = new Form1())
                tulemus = loginForm.ShowDialog();
            if (tulemus == DialogResult.OK)
            {
                this.Text =
                    Application.CompanyName + " - " + 
                    Application.ProductName + " " + 
                    Application.ProductVersion + " - " + 
                    Program.kasutaja.AmetJaNimi;
            }
            else
            {
            }
        }

        private void KasutajadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.kasutaja.Isikukood == null)
            {
                Login();
                if (Program.kasutaja.Isikukood != null)
                    KasutajadToolStripMenuItem_Click(sender, e);
            }
            else
            {
                if (Program.kasutaja.OnAdmin)
                {
                    AndmeteSisestus adminMDIChild = new AndmeteSisestus();
                    adminMDIChild.MdiParent = this;
                    adminMDIChild.Show();
                }
                else
                    MessageBox.Show($"Kasutajal '{Program.kasutaja.AmetJaNimi}' ei ole administraatori õigusi.", "Ei ole administraator", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void MuudatusteLogiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.kasutaja.Isikukood == null)
            {
                Login();
                MuudatusteLogiToolStripMenuItem_Click(sender, e);
            }
            else
            {
                if (Program.kasutaja.OnAdmin)
                {
                    Logid logidMDIChild = new Logid();
                    logidMDIChild.MdiParent = this;
                    logidMDIChild.Show();
                }
                else
                    MessageBox.Show($"Kasutajal '{Program.kasutaja.AmetJaNimi}' ei ole administraatori õigusi.", "Ei ole administraator", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }
    }
}
