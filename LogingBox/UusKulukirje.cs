﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace LogingBox
{
    public partial class UusKulukirje : Form
    {

        Failid t = new Failid();
        byte[] buff;
        string FailiNimi;
        //List<Failid> list;
        VaataFailics vf = new VaataFailics();
        public UusKulukirje()
        {
            InitializeComponent();
        }

        public Image ConvertBinaryToImage(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }




        public void btnAvaKuludokument_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { Filter = "JPG|*.jpg", ValidateNames = true, Multiselect = false };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                FailiNimi = ofd.FileName;
                lblFailinimi.Text = FailiNimi;
                buff = File.ReadAllBytes(FailiNimi);
                Failid fls = new Failid() { KulukirjeID = 1, Nimi = FailiNimi, Data = buff, ContentType = "image/JPEG" };
                Program.rhs.Failid.Add(fls);
                Program.rhs.SaveChanges();
                MessageBox.Show("Oled edukalt pildi lisanud kulukirjele", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                failidBindingSource.DataSource = Program.rhs.Failid.ToList();
            }


        }

        private void UusKulukirje_Load(object sender, EventArgs e)
        {
            failidBindingSource.DataSource = Program.rhs.Failid.ToList();
        }

        private void btnKustutaFail_Click(object sender, EventArgs e)
        {
            t = Program.rhs.Failid.Where(x => x.Id == ((Failid)dataGridViewFailid.CurrentRow.DataBoundItem).Id).Single();
            if (MessageBox.Show($"Kas oled kindel, et soovid kustutada '{t.Nimi}'?", "Kas oled kindel?",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Program.rhs.Failid.Remove(t);
                Program.rhs.SaveChanges();
                failidBindingSource.DataSource = Program.rhs.Failid.ToList();
                MessageBox.Show($"Fail'{t.Nimi}' on kustutatud!!!");

            }
        }

        private void btnVaataFaili_Click(object sender, EventArgs e)
        {
            //Form4 ur = new Form4();
            //ur.ShowDialog();
            Image img = Image.FromFile(lblFailinimi.Text);
            //Image imgdb = Image.
            VaataFailics frm = new VaataFailics();
            frm.ImageToShow = img;
            frm.ShowDialog();
            frm.Dispose();
        }

        //ne.Employees
        //        .OrderBy(x => x.EmployeeID)
        //        .Skip(8)
        //        .ToList()
        //        .ForEach(
        //        x =>
        //        {
        //    Bitmap b = new Bitmap(new MemoryStream(x.Photo.Skip(x.EmployeeID < 10 ? 78 : 0).ToArray()));
        //    Form1 f = new Form1();
        //    f.pictureBox1.Image = b;
        //    f.ShowDialog();
        //}
        //        );


        private void dataGridViewFailid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = dataGridViewFailid.Rows[rowIndex];
            FailiNimi = dataGridViewFailid.Rows[1].Cells[1].Value.ToString();// row.Cells[1].Value;
        }
    }
}
