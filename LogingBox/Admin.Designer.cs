﻿namespace LogingBox
{
    partial class AndmeteSisestus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SalvestanNupp = new System.Windows.Forms.Button();
            this.EesnimiLabel = new System.Windows.Forms.Label();
            this.PerekonnanimiLabel = new System.Windows.Forms.Label();
            this.AmetikohtLabel = new System.Windows.Forms.Label();
            this.OYLabel = new System.Windows.Forms.Label();
            this.RaamatupidajaCB1 = new System.Windows.Forms.CheckBox();
            this.AktiivneCB = new System.Windows.Forms.CheckBox();
            this.EesnimiBox = new System.Windows.Forms.TextBox();
            this.PerekonnanimiBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.IsikukoodLable = new System.Windows.Forms.Label();
            this.IsikukoodBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AmetikohtBox = new System.Windows.Forms.TextBox();
            this.IsikukoodiAbiLabel = new System.Windows.Forms.Label();
            this.YlemusBox1 = new System.Windows.Forms.ComboBox();
            this.KustutamiseButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // SalvestanNupp
            // 
            this.SalvestanNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.SalvestanNupp.Location = new System.Drawing.Point(146, 367);
            this.SalvestanNupp.Margin = new System.Windows.Forms.Padding(2);
            this.SalvestanNupp.Name = "SalvestanNupp";
            this.SalvestanNupp.Size = new System.Drawing.Size(202, 41);
            this.SalvestanNupp.TabIndex = 0;
            this.SalvestanNupp.Text = "Salvestan uue kasutaja";
            this.SalvestanNupp.UseVisualStyleBackColor = true;
            this.SalvestanNupp.Click += new System.EventHandler(this.SalvestanButton_Click);
            // 
            // EesnimiLabel
            // 
            this.EesnimiLabel.AutoSize = true;
            this.EesnimiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EesnimiLabel.Location = new System.Drawing.Point(34, 136);
            this.EesnimiLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.EesnimiLabel.Name = "EesnimiLabel";
            this.EesnimiLabel.Size = new System.Drawing.Size(61, 17);
            this.EesnimiLabel.TabIndex = 1;
            this.EesnimiLabel.Text = "Eesnimi:";
            this.EesnimiLabel.Click += new System.EventHandler(this.EesnimiLabel_Click);
            // 
            // PerekonnanimiLabel
            // 
            this.PerekonnanimiLabel.AutoSize = true;
            this.PerekonnanimiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PerekonnanimiLabel.Location = new System.Drawing.Point(34, 180);
            this.PerekonnanimiLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PerekonnanimiLabel.Name = "PerekonnanimiLabel";
            this.PerekonnanimiLabel.Size = new System.Drawing.Size(106, 17);
            this.PerekonnanimiLabel.TabIndex = 2;
            this.PerekonnanimiLabel.Text = "Perekonnanimi:";
            // 
            // AmetikohtLabel
            // 
            this.AmetikohtLabel.AutoSize = true;
            this.AmetikohtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AmetikohtLabel.Location = new System.Drawing.Point(34, 224);
            this.AmetikohtLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.AmetikohtLabel.Name = "AmetikohtLabel";
            this.AmetikohtLabel.Size = new System.Drawing.Size(74, 17);
            this.AmetikohtLabel.TabIndex = 3;
            this.AmetikohtLabel.Text = "Ametikoht:";
            this.AmetikohtLabel.Click += new System.EventHandler(this.AmetikohtLabel_Click);
            // 
            // OYLabel
            // 
            this.OYLabel.AutoSize = true;
            this.OYLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OYLabel.Location = new System.Drawing.Point(34, 271);
            this.OYLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.OYLabel.Name = "OYLabel";
            this.OYLabel.Size = new System.Drawing.Size(107, 17);
            this.OYLabel.TabIndex = 4;
            this.OYLabel.Text = "Otsene ülemus:";
            // 
            // RaamatupidajaCB1
            // 
            this.RaamatupidajaCB1.AutoSize = true;
            this.RaamatupidajaCB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RaamatupidajaCB1.Location = new System.Drawing.Point(224, 305);
            this.RaamatupidajaCB1.Margin = new System.Windows.Forms.Padding(2);
            this.RaamatupidajaCB1.Name = "RaamatupidajaCB1";
            this.RaamatupidajaCB1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RaamatupidajaCB1.Size = new System.Drawing.Size(15, 14);
            this.RaamatupidajaCB1.TabIndex = 6;
            this.RaamatupidajaCB1.UseVisualStyleBackColor = true;
            this.RaamatupidajaCB1.CheckedChanged += new System.EventHandler(this.True);
            // 
            // AktiivneCB
            // 
            this.AktiivneCB.AutoSize = true;
            this.AktiivneCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AktiivneCB.Location = new System.Drawing.Point(224, 336);
            this.AktiivneCB.Margin = new System.Windows.Forms.Padding(2);
            this.AktiivneCB.Name = "AktiivneCB";
            this.AktiivneCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.AktiivneCB.Size = new System.Drawing.Size(15, 14);
            this.AktiivneCB.TabIndex = 7;
            this.AktiivneCB.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.AktiivneCB.UseVisualStyleBackColor = true;
            this.AktiivneCB.CheckedChanged += new System.EventHandler(this.True);
            // 
            // EesnimiBox
            // 
            this.EesnimiBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EesnimiBox.Location = new System.Drawing.Point(155, 128);
            this.EesnimiBox.Margin = new System.Windows.Forms.Padding(2);
            this.EesnimiBox.Name = "EesnimiBox";
            this.EesnimiBox.Size = new System.Drawing.Size(193, 26);
            this.EesnimiBox.TabIndex = 2;
            this.EesnimiBox.TextChanged += new System.EventHandler(this.EesnimiBox_TextChanged);
            // 
            // PerekonnanimiBox
            // 
            this.PerekonnanimiBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PerekonnanimiBox.Location = new System.Drawing.Point(155, 172);
            this.PerekonnanimiBox.Margin = new System.Windows.Forms.Padding(2);
            this.PerekonnanimiBox.Name = "PerekonnanimiBox";
            this.PerekonnanimiBox.Size = new System.Drawing.Size(193, 26);
            this.PerekonnanimiBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 305);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kas on raamatupidaja?";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 333);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Kas on ettevõttes aktiivne?";
            // 
            // IsikukoodLable
            // 
            this.IsikukoodLable.AutoSize = true;
            this.IsikukoodLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsikukoodLable.Location = new System.Drawing.Point(38, 93);
            this.IsikukoodLable.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IsikukoodLable.Name = "IsikukoodLable";
            this.IsikukoodLable.Size = new System.Drawing.Size(71, 17);
            this.IsikukoodLable.TabIndex = 13;
            this.IsikukoodLable.Text = "Isikukood:";
            // 
            // IsikukoodBox
            // 
            this.IsikukoodBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsikukoodBox.Location = new System.Drawing.Point(155, 84);
            this.IsikukoodBox.Margin = new System.Windows.Forms.Padding(2);
            this.IsikukoodBox.Name = "IsikukoodBox";
            this.IsikukoodBox.Size = new System.Drawing.Size(193, 26);
            this.IsikukoodBox.TabIndex = 1;
            this.IsikukoodBox.TextChanged += new System.EventHandler(this.IsikukoodBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(340, 24);
            this.label3.TabIndex = 14;
            this.label3.Text = "Andmebaasi kasutajate manageerimine";
            // 
            // AmetikohtBox
            // 
            this.AmetikohtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AmetikohtBox.Location = new System.Drawing.Point(155, 216);
            this.AmetikohtBox.Margin = new System.Windows.Forms.Padding(2);
            this.AmetikohtBox.Name = "AmetikohtBox";
            this.AmetikohtBox.Size = new System.Drawing.Size(193, 26);
            this.AmetikohtBox.TabIndex = 4;
            // 
            // IsikukoodiAbiLabel
            // 
            this.IsikukoodiAbiLabel.AutoSize = true;
            this.IsikukoodiAbiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.IsikukoodiAbiLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.IsikukoodiAbiLabel.Location = new System.Drawing.Point(170, 112);
            this.IsikukoodiAbiLabel.Name = "IsikukoodiAbiLabel";
            this.IsikukoodiAbiLabel.Size = new System.Drawing.Size(148, 13);
            this.IsikukoodiAbiLabel.TabIndex = 16;
            this.IsikukoodiAbiLabel.Text = "See isikukood on juba olemas";
            this.IsikukoodiAbiLabel.Visible = false;
            this.IsikukoodiAbiLabel.Click += new System.EventHandler(this.Label4_Click);
            // 
            // YlemusBox1
            // 
            this.YlemusBox1.FormattingEnabled = true;
            this.YlemusBox1.Location = new System.Drawing.Point(155, 267);
            this.YlemusBox1.Name = "YlemusBox1";
            this.YlemusBox1.Size = new System.Drawing.Size(193, 21);
            this.YlemusBox1.TabIndex = 18;
            this.YlemusBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // KustutamiseButton
            // 
            this.KustutamiseButton.Location = new System.Drawing.Point(857, 375);
            this.KustutamiseButton.Name = "KustutamiseButton";
            this.KustutamiseButton.Size = new System.Drawing.Size(75, 23);
            this.KustutamiseButton.TabIndex = 23;
            this.KustutamiseButton.Text = "Kustuta";
            this.KustutamiseButton.UseVisualStyleBackColor = true;
            this.KustutamiseButton.Click += new System.EventHandler(this.KustutamiseButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(395, 20);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(857, 349);
            this.dataGridView1.TabIndex = 24;
            this.dataGridView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DataGridView1_MouseDoubleClick);
            // 
            // AndmeteSisestus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 419);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.KustutamiseButton);
            this.Controls.Add(this.YlemusBox1);
            this.Controls.Add(this.IsikukoodiAbiLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IsikukoodBox);
            this.Controls.Add(this.IsikukoodLable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AmetikohtBox);
            this.Controls.Add(this.PerekonnanimiBox);
            this.Controls.Add(this.EesnimiBox);
            this.Controls.Add(this.AktiivneCB);
            this.Controls.Add(this.RaamatupidajaCB1);
            this.Controls.Add(this.OYLabel);
            this.Controls.Add(this.AmetikohtLabel);
            this.Controls.Add(this.PerekonnanimiLabel);
            this.Controls.Add(this.EesnimiLabel);
            this.Controls.Add(this.SalvestanNupp);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AndmeteSisestus";
            this.Text = "Kasutajate administreerimine";
            this.Load += new System.EventHandler(this.AndmeteSisestus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SalvestanNupp;
        private System.Windows.Forms.Label EesnimiLabel;
        private System.Windows.Forms.Label PerekonnanimiLabel;
        private System.Windows.Forms.Label AmetikohtLabel;
        private System.Windows.Forms.Label OYLabel;
        private System.Windows.Forms.CheckBox RaamatupidajaCB1;
        private System.Windows.Forms.CheckBox AktiivneCB;
        private System.Windows.Forms.TextBox EesnimiBox;
        private System.Windows.Forms.TextBox PerekonnanimiBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label IsikukoodLable;
        private System.Windows.Forms.TextBox IsikukoodBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AmetikohtBox;
        private System.Windows.Forms.Label IsikukoodiAbiLabel;
        private System.Windows.Forms.ComboBox YlemusBox1;
        private System.Windows.Forms.Button KustutamiseButton;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}