﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace LogingBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ToolTip1.SetToolTip(textBox1, "isikukood");
            ToolTip1.SetToolTip(textBox2, "salasõna");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // if (CheckIsik)
            Töötaja k = Program.rhs.Töötaja.Where(x => x.Isikukood == textBox1.Text).SingleOrDefault();
            if (k != null)
                if (k.Salas6na.Trim() == textBox2.Text)
                    if (k.OnAktiivne)
                    {
                        DialogResult = DialogResult.OK;
                        Program.kasutaja = k;
                    }
                    else
                    {
                        MessageBox.Show("Sinu kasutajakonto ei ole aktiivne. Pöördu administraatori poole", "Kasutajakonto ei ole aktiivne", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                else
                {
                    MessageBox.Show("Palun kontrolli oma kasutajanime ja salasõna", "Kasutajanimi ja/või parool vale", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            else
            {
                MessageBox.Show("Palun kontrolli oma kasutajanime ja salasõna", "Kasutajanimi ja/või parool vale", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void TühistaButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Kas oled kindel, et ei soovi sisse logida või kasutajat vahetada?", "Ei soovi sisse logida ega kasutajatat vahetada",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
