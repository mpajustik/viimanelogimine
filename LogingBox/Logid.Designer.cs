﻿namespace LogingBox
{
    partial class Logid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IdTextBox1 = new System.Windows.Forms.TextBox();
            this.KlassTextBox2 = new System.Windows.Forms.TextBox();
            this.InstantsTextBox3 = new System.Windows.Forms.TextBox();
            this.PropertyTextBox4 = new System.Windows.Forms.TextBox();
            this.VanaTextBox5 = new System.Windows.Forms.TextBox();
            this.UusTextBox6 = new System.Windows.Forms.TextBox();
            this.KunasTextBox7 = new System.Windows.Forms.TextBox();
            this.KesTextBox8 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FilterTühjaksButton1 = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(28, 49);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1026, 726);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseLeave);
            // 
            // IdTextBox1
            // 
            this.IdTextBox1.Location = new System.Drawing.Point(79, 23);
            this.IdTextBox1.Name = "IdTextBox1";
            this.IdTextBox1.Size = new System.Drawing.Size(100, 20);
            this.IdTextBox1.TabIndex = 1;
            this.IdTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.IdTextBox1.TextChanged += new System.EventHandler(this.IdTextBox1_TextChanged);
            // 
            // KlassTextBox2
            // 
            this.KlassTextBox2.Location = new System.Drawing.Point(200, 23);
            this.KlassTextBox2.Name = "KlassTextBox2";
            this.KlassTextBox2.Size = new System.Drawing.Size(100, 20);
            this.KlassTextBox2.TabIndex = 2;
            this.KlassTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.KlassTextBox2.TextChanged += new System.EventHandler(this.KlassTextBox2_TextChanged);
            // 
            // InstantsTextBox3
            // 
            this.InstantsTextBox3.Location = new System.Drawing.Point(320, 23);
            this.InstantsTextBox3.Name = "InstantsTextBox3";
            this.InstantsTextBox3.Size = new System.Drawing.Size(100, 20);
            this.InstantsTextBox3.TabIndex = 3;
            this.InstantsTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.InstantsTextBox3.TextChanged += new System.EventHandler(this.InstantsTextBox3_TextChanged);
            // 
            // PropertyTextBox4
            // 
            this.PropertyTextBox4.Location = new System.Drawing.Point(442, 23);
            this.PropertyTextBox4.Name = "PropertyTextBox4";
            this.PropertyTextBox4.Size = new System.Drawing.Size(100, 20);
            this.PropertyTextBox4.TabIndex = 4;
            this.PropertyTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PropertyTextBox4.TextChanged += new System.EventHandler(this.PropertyTextBox4_TextChanged);
            // 
            // VanaTextBox5
            // 
            this.VanaTextBox5.Location = new System.Drawing.Point(568, 23);
            this.VanaTextBox5.Name = "VanaTextBox5";
            this.VanaTextBox5.Size = new System.Drawing.Size(100, 20);
            this.VanaTextBox5.TabIndex = 5;
            this.VanaTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.VanaTextBox5.TextChanged += new System.EventHandler(this.VanaTextBox5_TextChanged);
            // 
            // UusTextBox6
            // 
            this.UusTextBox6.Location = new System.Drawing.Point(684, 23);
            this.UusTextBox6.Name = "UusTextBox6";
            this.UusTextBox6.Size = new System.Drawing.Size(100, 20);
            this.UusTextBox6.TabIndex = 6;
            this.UusTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.UusTextBox6.TextChanged += new System.EventHandler(this.UusTextBox6_TextChanged);
            // 
            // KunasTextBox7
            // 
            this.KunasTextBox7.Location = new System.Drawing.Point(804, 23);
            this.KunasTextBox7.Name = "KunasTextBox7";
            this.KunasTextBox7.Size = new System.Drawing.Size(100, 20);
            this.KunasTextBox7.TabIndex = 7;
            this.KunasTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.KunasTextBox7.TextChanged += new System.EventHandler(this.KunasTextBox7_TextChanged);
            // 
            // KesTextBox8
            // 
            this.KesTextBox8.Location = new System.Drawing.Point(929, 23);
            this.KesTextBox8.Name = "KesTextBox8";
            this.KesTextBox8.Size = new System.Drawing.Size(100, 20);
            this.KesTextBox8.TabIndex = 8;
            this.KesTextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.KesTextBox8.TextChanged += new System.EventHandler(this.KesTextBox8_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(27, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Filtrid:";
            // 
            // FilterTühjaksButton1
            // 
            this.FilterTühjaksButton1.Location = new System.Drawing.Point(1074, 49);
            this.FilterTühjaksButton1.Name = "FilterTühjaksButton1";
            this.FilterTühjaksButton1.Size = new System.Drawing.Size(39, 21);
            this.FilterTühjaksButton1.TabIndex = 10;
            this.FilterTühjaksButton1.Text = "Clr";
            this.FilterTühjaksButton1.UseVisualStyleBackColor = true;
            this.FilterTühjaksButton1.Click += new System.EventHandler(this.FilterTühjaksButton1_Click);
            // 
            // Label2
            // 
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Label2.Location = new System.Drawing.Point(1053, 21);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(82, 20);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "label2";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Logid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 787);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.FilterTühjaksButton1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.KesTextBox8);
            this.Controls.Add(this.KunasTextBox7);
            this.Controls.Add(this.UusTextBox6);
            this.Controls.Add(this.VanaTextBox5);
            this.Controls.Add(this.PropertyTextBox4);
            this.Controls.Add(this.InstantsTextBox3);
            this.Controls.Add(this.KlassTextBox2);
            this.Controls.Add(this.IdTextBox1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Logid";
            this.Text = "Logid";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox IdTextBox1;
        private System.Windows.Forms.TextBox KlassTextBox2;
        private System.Windows.Forms.TextBox InstantsTextBox3;
        private System.Windows.Forms.TextBox PropertyTextBox4;
        private System.Windows.Forms.TextBox VanaTextBox5;
        private System.Windows.Forms.TextBox UusTextBox6;
        private System.Windows.Forms.TextBox KunasTextBox7;
        private System.Windows.Forms.TextBox KesTextBox8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button FilterTühjaksButton1;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}