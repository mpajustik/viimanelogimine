//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogingBox
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kulukirjed
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Kulukirjed()
        {
            this.Failid = new HashSet<Failid>();
        }
    
        public int Id { get; set; }
        public int kuluaruanne { get; set; }
        public int kululiik { get; set; }
        public string teenusepakkuja { get; set; }
        public Nullable<short> kogus { get; set; }
        public int kogukulu { get; set; }
        public byte[] dokumendid { get; set; }
    
        public virtual KuluAruanne KuluAruanne { get; set; }
        public virtual Kululiigid Kululiigid { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Failid> Failid { get; set; }
    }
}
