
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/21/2017 13:31:54
-- Generated from EDMX file: C:\Users\merli\source\repos\Sisselogiminejatabelissesalvestamine\LogingBox\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ReisikuludeHaldamiseSysteemVM];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_KuluAruanne]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[KuluAruanne] DROP CONSTRAINT [FK_KuluAruanne];
GO
IF OBJECT_ID(N'[dbo].[FK_Kulukirjed_KuluArunne]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Kulukirjed] DROP CONSTRAINT [FK_Kulukirjed_KuluArunne];
GO
IF OBJECT_ID(N'[dbo].[FK_Kulukirjed_Kululiik]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Kulukirjed] DROP CONSTRAINT [FK_Kulukirjed_Kululiik];
GO
IF OBJECT_ID(N'[dbo].[FK_OtseneÜlemus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Töötaja] DROP CONSTRAINT [FK_OtseneÜlemus];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AruandeOlek]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AruandeOlek];
GO
IF OBJECT_ID(N'[dbo].[KuluAruanne]', 'U') IS NOT NULL
    DROP TABLE [dbo].[KuluAruanne];
GO
IF OBJECT_ID(N'[dbo].[Kulukirjed]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Kulukirjed];
GO
IF OBJECT_ID(N'[dbo].[Kululiigid]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Kululiigid];
GO
IF OBJECT_ID(N'[dbo].[Muudatused]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Muudatused];
GO
IF OBJECT_ID(N'[dbo].[Reis]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Reis];
GO
IF OBJECT_ID(N'[dbo].[SisselogimiseTabel]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SisselogimiseTabel];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[Töötaja]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Töötaja];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AruandeOlek'
CREATE TABLE [dbo].[AruandeOlek] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Olek] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'KuluAruanne'
CREATE TABLE [dbo].[KuluAruanne] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [koostaja] char(11)  NOT NULL,
    [otsene_juht] char(11)  NOT NULL,
    [reisi_kirjeldus] nvarchar(70)  NOT NULL,
    [reisi_päevade_arv] smallint  NOT NULL,
    [olek] int  NOT NULL,
    [reisi_algus] datetime  NOT NULL,
    [reisi_lõpp] datetime  NOT NULL
);
GO

-- Creating table 'Kulukirjed'
CREATE TABLE [dbo].[Kulukirjed] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [kuluaruanne] int  NOT NULL,
    [kululiik] int  NOT NULL,
    [teenusepakkuja] nvarchar(30)  NULL,
    [kogus] smallint  NULL,
    [kogukulu] int  NOT NULL,
    [dokumendid] varbinary(max)  NULL
);
GO

-- Creating table 'Kululiigid'
CREATE TABLE [dbo].[Kululiigid] (
    [Id] int  NOT NULL,
    [liik] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'Muudatused'
CREATE TABLE [dbo].[Muudatused] (
    [Id] int  NOT NULL,
    [Kommentaar] nvarchar(150)  NOT NULL,
    [Kuupäev] datetime  NOT NULL,
    [IsikuID] nchar(10)  NOT NULL
);
GO

-- Creating table 'Reis'
CREATE TABLE [dbo].[Reis] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Sihtkoht] nvarchar(50)  NOT NULL,
    [ReisiAlgus] datetime  NOT NULL,
    [ReisiLopp] datetime  NOT NULL,
    [ReisiKirjeldus] nvarchar(150)  NOT NULL
);
GO

-- Creating table 'SisselogimiseTabel'
CREATE TABLE [dbo].[SisselogimiseTabel] (
    [Kasutajanimi] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NOT NULL,
    [Role] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Töötaja'
CREATE TABLE [dbo].[Töötaja] (
    [Isikukood] char(11)  NOT NULL,
    [Eesnimi] nvarchar(20)  NOT NULL,
    [Perekonnanimi] nvarchar(50)  NOT NULL,
    [Ametikoht] nchar(30)  NULL,
    [OtseneÜlemus] char(11)  NULL,
    [OnRaamatupidaja] bit  NOT NULL,
    [OnAktiivne] bit  NOT NULL,
    [OnAdmin] bit  NOT NULL,
    [Salas6na] nchar(20)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'AruandeOlek'
ALTER TABLE [dbo].[AruandeOlek]
ADD CONSTRAINT [PK_AruandeOlek]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'KuluAruanne'
ALTER TABLE [dbo].[KuluAruanne]
ADD CONSTRAINT [PK_KuluAruanne]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Kulukirjed'
ALTER TABLE [dbo].[Kulukirjed]
ADD CONSTRAINT [PK_Kulukirjed]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Kululiigid'
ALTER TABLE [dbo].[Kululiigid]
ADD CONSTRAINT [PK_Kululiigid]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Muudatused'
ALTER TABLE [dbo].[Muudatused]
ADD CONSTRAINT [PK_Muudatused]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Reis'
ALTER TABLE [dbo].[Reis]
ADD CONSTRAINT [PK_Reis]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Kasutajanimi] in table 'SisselogimiseTabel'
ALTER TABLE [dbo].[SisselogimiseTabel]
ADD CONSTRAINT [PK_SisselogimiseTabel]
    PRIMARY KEY CLUSTERED ([Kasutajanimi] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [Isikukood] in table 'Töötaja'
ALTER TABLE [dbo].[Töötaja]
ADD CONSTRAINT [PK_Töötaja]
    PRIMARY KEY CLUSTERED ([Isikukood] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [olek] in table 'KuluAruanne'
ALTER TABLE [dbo].[KuluAruanne]
ADD CONSTRAINT [FK_KuluAruanne]
    FOREIGN KEY ([olek])
    REFERENCES [dbo].[AruandeOlek]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_KuluAruanne'
CREATE INDEX [IX_FK_KuluAruanne]
ON [dbo].[KuluAruanne]
    ([olek]);
GO

-- Creating foreign key on [kuluaruanne] in table 'Kulukirjed'
ALTER TABLE [dbo].[Kulukirjed]
ADD CONSTRAINT [FK_Kulukirjed_KuluArunne]
    FOREIGN KEY ([kuluaruanne])
    REFERENCES [dbo].[KuluAruanne]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Kulukirjed_KuluArunne'
CREATE INDEX [IX_FK_Kulukirjed_KuluArunne]
ON [dbo].[Kulukirjed]
    ([kuluaruanne]);
GO

-- Creating foreign key on [kululiik] in table 'Kulukirjed'
ALTER TABLE [dbo].[Kulukirjed]
ADD CONSTRAINT [FK_Kulukirjed_Kululiik]
    FOREIGN KEY ([kululiik])
    REFERENCES [dbo].[Kululiigid]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Kulukirjed_Kululiik'
CREATE INDEX [IX_FK_Kulukirjed_Kululiik]
ON [dbo].[Kulukirjed]
    ([kululiik]);
GO

-- Creating foreign key on [OtseneÜlemus] in table 'Töötaja'
ALTER TABLE [dbo].[Töötaja]
ADD CONSTRAINT [FK_TöötajaÜlemus]
    FOREIGN KEY ([OtseneÜlemus])
    REFERENCES [dbo].[Töötaja]
        ([Isikukood])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TöötajaÜlemus'
CREATE INDEX [IX_FK_TöötajaÜlemus]
ON [dbo].[Töötaja]
    ([OtseneÜlemus]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------