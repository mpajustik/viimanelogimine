﻿namespace LogingBox
{
    partial class UusKulukirje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblTeenusepakkuja = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblKululiik = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblKogus = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.lblKogukulu = new System.Windows.Forms.Label();
            this.btnAvaKuludokument = new System.Windows.Forms.Button();
            this.btnSalvestaKulukirje = new System.Windows.Forms.Button();
            this.lblKulukirjeIDNimi = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblKulukirjeIDNumber = new System.Windows.Forms.Label();
            this.lblKuluaruandeIDNumber = new System.Windows.Forms.Label();
            this.lblFailinimi = new System.Windows.Forms.Label();
            this.dataGridViewFailid = new System.Windows.Forms.DataGridView();
            this.btnKustutaFail = new System.Windows.Forms.Button();
            this.btnVaataFaili = new System.Windows.Forms.Button();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kulukirjeIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nimiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.contentTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.failidBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFailid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failidBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(189, 164);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(289, 22);
            this.textBox1.TabIndex = 0;
            // 
            // lblTeenusepakkuja
            // 
            this.lblTeenusepakkuja.AutoSize = true;
            this.lblTeenusepakkuja.Location = new System.Drawing.Point(41, 164);
            this.lblTeenusepakkuja.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeenusepakkuja.Name = "lblTeenusepakkuja";
            this.lblTeenusepakkuja.Size = new System.Drawing.Size(113, 17);
            this.lblTeenusepakkuja.TabIndex = 1;
            this.lblTeenusepakkuja.Text = "Teenusepakkuja";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(189, 111);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(289, 24);
            this.comboBox1.TabIndex = 2;
            // 
            // lblKululiik
            // 
            this.lblKululiik.AutoSize = true;
            this.lblKululiik.Location = new System.Drawing.Point(41, 121);
            this.lblKululiik.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKululiik.Name = "lblKululiik";
            this.lblKululiik.Size = new System.Drawing.Size(52, 17);
            this.lblKululiik.TabIndex = 1;
            this.lblKululiik.Text = "Kululiik";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(189, 218);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(289, 22);
            this.textBox2.TabIndex = 0;
            // 
            // lblKogus
            // 
            this.lblKogus.AutoSize = true;
            this.lblKogus.Location = new System.Drawing.Point(41, 218);
            this.lblKogus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKogus.Name = "lblKogus";
            this.lblKogus.Size = new System.Drawing.Size(48, 17);
            this.lblKogus.TabIndex = 1;
            this.lblKogus.Text = "Kogus";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(189, 281);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(289, 22);
            this.textBox3.TabIndex = 0;
            // 
            // lblKogukulu
            // 
            this.lblKogukulu.AutoSize = true;
            this.lblKogukulu.Location = new System.Drawing.Point(41, 281);
            this.lblKogukulu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKogukulu.Name = "lblKogukulu";
            this.lblKogukulu.Size = new System.Drawing.Size(67, 17);
            this.lblKogukulu.TabIndex = 1;
            this.lblKogukulu.Text = "Kogukulu";
            // 
            // btnAvaKuludokument
            // 
            this.btnAvaKuludokument.Location = new System.Drawing.Point(563, 29);
            this.btnAvaKuludokument.Margin = new System.Windows.Forms.Padding(4);
            this.btnAvaKuludokument.Name = "btnAvaKuludokument";
            this.btnAvaKuludokument.Size = new System.Drawing.Size(172, 39);
            this.btnAvaKuludokument.TabIndex = 3;
            this.btnAvaKuludokument.Text = "Lisa kuludokument";
            this.btnAvaKuludokument.UseVisualStyleBackColor = true;
            this.btnAvaKuludokument.Click += new System.EventHandler(this.btnAvaKuludokument_Click);
            // 
            // btnSalvestaKulukirje
            // 
            this.btnSalvestaKulukirje.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.btnSalvestaKulukirje.Location = new System.Drawing.Point(119, 571);
            this.btnSalvestaKulukirje.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalvestaKulukirje.Name = "btnSalvestaKulukirje";
            this.btnSalvestaKulukirje.Size = new System.Drawing.Size(361, 66);
            this.btnSalvestaKulukirje.TabIndex = 4;
            this.btnSalvestaKulukirje.Text = "Salvesta kulukirje";
            this.btnSalvestaKulukirje.UseVisualStyleBackColor = true;
            // 
            // lblKulukirjeIDNimi
            // 
            this.lblKulukirjeIDNimi.AutoSize = true;
            this.lblKulukirjeIDNimi.Location = new System.Drawing.Point(41, 16);
            this.lblKulukirjeIDNimi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKulukirjeIDNimi.Name = "lblKulukirjeIDNimi";
            this.lblKulukirjeIDNimi.Size = new System.Drawing.Size(79, 17);
            this.lblKulukirjeIDNimi.TabIndex = 5;
            this.lblKulukirjeIDNimi.Text = "Kulukirje ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Kuluaruande ID";
            // 
            // lblKulukirjeIDNumber
            // 
            this.lblKulukirjeIDNumber.AutoSize = true;
            this.lblKulukirjeIDNumber.Location = new System.Drawing.Point(185, 16);
            this.lblKulukirjeIDNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKulukirjeIDNumber.Name = "lblKulukirjeIDNumber";
            this.lblKulukirjeIDNumber.Size = new System.Drawing.Size(81, 17);
            this.lblKulukirjeIDNumber.TabIndex = 5;
            this.lblKulukirjeIDNumber.Text = "Kulukirje Nr";
            // 
            // lblKuluaruandeIDNumber
            // 
            this.lblKuluaruandeIDNumber.AutoSize = true;
            this.lblKuluaruandeIDNumber.Location = new System.Drawing.Point(185, 60);
            this.lblKuluaruandeIDNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKuluaruandeIDNumber.Name = "lblKuluaruandeIDNumber";
            this.lblKuluaruandeIDNumber.Size = new System.Drawing.Size(113, 17);
            this.lblKuluaruandeIDNumber.TabIndex = 5;
            this.lblKuluaruandeIDNumber.Text = "Kuluaruande NR";
            // 
            // lblFailinimi
            // 
            this.lblFailinimi.AutoSize = true;
            this.lblFailinimi.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.failidBindingSource, "Nimi", true));
            this.lblFailinimi.Location = new System.Drawing.Point(16, 388);
            this.lblFailinimi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFailinimi.Name = "lblFailinimi";
            this.lblFailinimi.Size = new System.Drawing.Size(58, 17);
            this.lblFailinimi.TabIndex = 7;
            this.lblFailinimi.Text = "Failinimi";
            // 
            // dataGridViewFailid
            // 
            this.dataGridViewFailid.AutoGenerateColumns = false;
            this.dataGridViewFailid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFailid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.kulukirjeIDDataGridViewTextBoxColumn,
            this.nimiDataGridViewTextBoxColumn,
            this.dataDataGridViewImageColumn,
            this.contentTypeDataGridViewTextBoxColumn});
            this.dataGridViewFailid.DataSource = this.failidBindingSource;
            this.dataGridViewFailid.Location = new System.Drawing.Point(563, 93);
            this.dataGridViewFailid.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewFailid.Name = "dataGridViewFailid";
            this.dataGridViewFailid.Size = new System.Drawing.Size(748, 544);
            this.dataGridViewFailid.TabIndex = 9;
            this.dataGridViewFailid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFailid_CellContentClick);
            // 
            // btnKustutaFail
            // 
            this.btnKustutaFail.Location = new System.Drawing.Point(754, 29);
            this.btnKustutaFail.Margin = new System.Windows.Forms.Padding(4);
            this.btnKustutaFail.Name = "btnKustutaFail";
            this.btnKustutaFail.Size = new System.Drawing.Size(171, 39);
            this.btnKustutaFail.TabIndex = 10;
            this.btnKustutaFail.Text = "Kustuta kuludokument";
            this.btnKustutaFail.UseVisualStyleBackColor = true;
            this.btnKustutaFail.Click += new System.EventHandler(this.btnKustutaFail_Click);
            // 
            // btnVaataFaili
            // 
            this.btnVaataFaili.Location = new System.Drawing.Point(947, 29);
            this.btnVaataFaili.Name = "btnVaataFaili";
            this.btnVaataFaili.Size = new System.Drawing.Size(171, 39);
            this.btnVaataFaili.TabIndex = 11;
            this.btnVaataFaili.Text = "Vaata faili";
            this.btnVaataFaili.UseVisualStyleBackColor = true;
            this.btnVaataFaili.Click += new System.EventHandler(this.btnVaataFaili_Click);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // kulukirjeIDDataGridViewTextBoxColumn
            // 
            this.kulukirjeIDDataGridViewTextBoxColumn.DataPropertyName = "KulukirjeID";
            this.kulukirjeIDDataGridViewTextBoxColumn.HeaderText = "KulukirjeID";
            this.kulukirjeIDDataGridViewTextBoxColumn.Name = "kulukirjeIDDataGridViewTextBoxColumn";
            this.kulukirjeIDDataGridViewTextBoxColumn.Width = 50;
            // 
            // nimiDataGridViewTextBoxColumn
            // 
            this.nimiDataGridViewTextBoxColumn.DataPropertyName = "Nimi";
            this.nimiDataGridViewTextBoxColumn.HeaderText = "Nimi";
            this.nimiDataGridViewTextBoxColumn.Name = "nimiDataGridViewTextBoxColumn";
            this.nimiDataGridViewTextBoxColumn.Width = 200;
            // 
            // dataDataGridViewImageColumn
            // 
            this.dataDataGridViewImageColumn.DataPropertyName = "Data";
            this.dataDataGridViewImageColumn.HeaderText = "Data";
            this.dataDataGridViewImageColumn.Name = "dataDataGridViewImageColumn";
            this.dataDataGridViewImageColumn.ReadOnly = true;
            // 
            // contentTypeDataGridViewTextBoxColumn
            // 
            this.contentTypeDataGridViewTextBoxColumn.DataPropertyName = "ContentType";
            this.contentTypeDataGridViewTextBoxColumn.HeaderText = "ContentType";
            this.contentTypeDataGridViewTextBoxColumn.Name = "contentTypeDataGridViewTextBoxColumn";
            // 
            // failidBindingSource
            // 
            this.failidBindingSource.DataSource = typeof(LogingBox.Failid);
            // 
            // UusKulukirje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1324, 652);
            this.Controls.Add(this.btnVaataFaili);
            this.Controls.Add(this.btnKustutaFail);
            this.Controls.Add(this.dataGridViewFailid);
            this.Controls.Add(this.lblFailinimi);
            this.Controls.Add(this.lblKuluaruandeIDNumber);
            this.Controls.Add(this.lblKulukirjeIDNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblKulukirjeIDNimi);
            this.Controls.Add(this.btnSalvestaKulukirje);
            this.Controls.Add(this.btnAvaKuludokument);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lblKululiik);
            this.Controls.Add(this.lblKogukulu);
            this.Controls.Add(this.lblKogus);
            this.Controls.Add(this.lblTeenusepakkuja);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UusKulukirje";
            this.Text = "Kulukirjete lisamine";
            this.Load += new System.EventHandler(this.UusKulukirje_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFailid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failidBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblTeenusepakkuja;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lblKululiik;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblKogus;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label lblKogukulu;
        private System.Windows.Forms.Button btnAvaKuludokument;
        private System.Windows.Forms.Button btnSalvestaKulukirje;
        private System.Windows.Forms.Label lblKulukirjeIDNimi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblKulukirjeIDNumber;
        private System.Windows.Forms.Label lblKuluaruandeIDNumber;
        private System.Windows.Forms.Label lblFailinimi;
        private System.Windows.Forms.DataGridView dataGridViewFailid;
        private System.Windows.Forms.BindingSource failidBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kulukirjeIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nimiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn dataDataGridViewImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contentTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnKustutaFail;
        private System.Windows.Forms.Button btnVaataFaili;
    }
}