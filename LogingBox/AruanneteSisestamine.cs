﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogingBox
{
    public partial class AruanneteSisestamine : Form
    {
        int kuluAruandeId;
        public AruanneteSisestamine()
        {
            InitializeComponent();
        }

        private void btnUus_Click(object sender, EventArgs e)
        {
            using (UueSisestamine frm = new UueSisestamine(new KuluAruanne()))
            {
                if (frm.ShowDialog()==DialogResult.OK)
                {
                    try
                    {
                        kuluAruanneBindingSource.Add(frm.KuluAruandeInfo);
                        Program.rhs.KuluAruanne.Add(frm.KuluAruandeInfo);
                        Program.rhs.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, "Message",MessageBoxButtons.OK,MessageBoxIcon.Error );
                    }
                }
            }
        }

        private void AruanneteSisestamine_Load(object sender, EventArgs e)
        {
            kuluAruanneBindingSource.DataSource = Program.rhs.KuluAruanne.ToList();
            aruandeOlekBindingSource.DataSource = Program.rhs.AruandeOlek.ToList();
            kulukirjedBindingSource.DataSource = Program.rhs.Kulukirjed.ToList();
            kululiigidBindingSource.DataSource = Program.rhs.Kululiigid.ToList();
            
        }
        //SEDA EI OLE VÕIB ÄRA KUSTUTADA
        //private void btnUuenda_Click(object sender, EventArgs e)
        //{
        //    kuluAruanneBindingSource.DataSource = Program.rhs.KuluAruanne.ToList();
        //    aruandeOlekBindingSource.DataSource = Program.rhs.AruandeOlek.ToList();
        //}

        private void btnMuuda_Click(object sender, EventArgs e)
        {
            KuluAruanne obj = kuluAruanneBindingSource.Current as KuluAruanne;
            if (obj != null)
            {
                using (UueSisestamine frm = new UueSisestamine(obj))
                {
                    if(frm.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            kuluAruanneBindingSource.EndEdit();
                            Program.rhs.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show(ex.Message,"Message",MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }

            }
        }

        private void btnKustuta_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Oled Sa kindel, et soovid kustutada aruande '{kuluAruandeId}'?", "Message", MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
            {
                int rows = dataGridAruanneteSisestamiseKast.RowCount;
                for (int i = rows - 1; i > 0; i--)
                {
                    if (dataGridAruanneteSisestamiseKast.Rows[i].Selected)
                    {
                        Program.rhs.KuluAruanne.Remove(dataGridAruanneteSisestamiseKast.Rows[i].DataBoundItem as KuluAruanne);
                        kuluAruanneBindingSource.RemoveAt(dataGridAruanneteSisestamiseKast.Rows[i].Index);
                    }
                }
                //kuluAruanneBindingSource.EndEdit();
                Program.rhs.SaveChanges();

            }
        }

        private void dataGridAruanneteSisestamiseKast_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            kuluAruandeId = Program.rhs.KuluAruanne.Where(x => x.Id == ((KuluAruanne)dataGridAruanneteSisestamiseKast.CurrentRow.DataBoundItem).Id).Single().Id;
            dataGridAruannetKastisKulukirjed.DataSource = Program.rhs.Kulukirjed.Where(x => x.KuluAruanne.Id == kuluAruandeId).ToList();
        }
    }
}
