﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace LogingBox
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        internal static ReisikuludeHaldamiseSysteemVMEntities1 rhs = new ReisikuludeHaldamiseSysteemVMEntities1();
        public static Töötaja kasutaja = new Töötaja();

        [STAThread]
        static void Main()
        {
            // ParandaIsikukoodid.IsikukoodideParandaja(rhs);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MDIParent1());
        }

    }
    partial class Töötaja
    {
        public string AmetJaNimi
        {
            get => $"{Ametikoht.Trim()} {Eesnimi.Trim()} {Perekonnanimi.Trim()}";
        }
    }
}
