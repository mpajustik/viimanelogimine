﻿namespace LogingBox
{
    partial class UueSisestamine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbKoostaja = new System.Windows.Forms.Label();
            this.lbOtseneYlemus = new System.Windows.Forms.Label();
            this.lbReisiKirjeldus = new System.Windows.Forms.Label();
            this.txtBoxReisiKirjeldus = new System.Windows.Forms.TextBox();
            this.lbReisiAlgus = new System.Windows.Forms.Label();
            this.lbReisiLopp = new System.Windows.Forms.Label();
            this.lbReisiPaevadeArv = new System.Windows.Forms.Label();
            this.txtBoxReisipaevadArv = new System.Windows.Forms.TextBox();
            this.lbOlek = new System.Windows.Forms.Label();
            this.comboBoxAruandeOlek = new System.Windows.Forms.ComboBox();
            this.btnSalvestaAruanne = new System.Windows.Forms.Button();
            this.dtpReisiAlgus = new System.Windows.Forms.DateTimePicker();
            this.dtpReisiLopp = new System.Windows.Forms.DateTimePicker();
            this.lbOtseseJuhiIsikukood = new System.Windows.Forms.Label();
            this.lbKoostajaIsikukood = new System.Windows.Forms.Label();
            this.datagridUueAruandeSisestamiseFormis = new System.Windows.Forms.DataGridView();
            this.btnUusKulukirje = new System.Windows.Forms.Button();
            this.lblKulukirjedTeks = new System.Windows.Forms.Label();
            this.btnMuudaKulukirje = new System.Windows.Forms.Button();
            this.btnKustutaKulukirje = new System.Windows.Forms.Button();
            this.lblAruandeId = new System.Windows.Forms.Label();
            this.lblKuluaruandeIDNumber = new System.Windows.Forms.Label();
            this.bindingSourceKuluAruanne = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kuluaruanneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kululiikDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.kululiigidBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teenusepakkujaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kogusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kogukuluDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dokumendidDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.kulukirjedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.datagridUueAruandeSisestamiseFormis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceKuluAruanne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kululiigidBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kulukirjedBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lbKoostaja
            // 
            this.lbKoostaja.AutoSize = true;
            this.lbKoostaja.Location = new System.Drawing.Point(24, 74);
            this.lbKoostaja.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbKoostaja.Name = "lbKoostaja";
            this.lbKoostaja.Size = new System.Drawing.Size(48, 13);
            this.lbKoostaja.TabIndex = 0;
            this.lbKoostaja.Text = "Koostaja";
            // 
            // lbOtseneYlemus
            // 
            this.lbOtseneYlemus.AutoSize = true;
            this.lbOtseneYlemus.Location = new System.Drawing.Point(24, 112);
            this.lbOtseneYlemus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbOtseneYlemus.Name = "lbOtseneYlemus";
            this.lbOtseneYlemus.Size = new System.Drawing.Size(79, 13);
            this.lbOtseneYlemus.TabIndex = 0;
            this.lbOtseneYlemus.Text = "Otsene Ülemus";
            // 
            // lbReisiKirjeldus
            // 
            this.lbReisiKirjeldus.AutoSize = true;
            this.lbReisiKirjeldus.Location = new System.Drawing.Point(24, 152);
            this.lbReisiKirjeldus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbReisiKirjeldus.Name = "lbReisiKirjeldus";
            this.lbReisiKirjeldus.Size = new System.Drawing.Size(71, 13);
            this.lbReisiKirjeldus.TabIndex = 0;
            this.lbReisiKirjeldus.Text = "Reisi kirjeldus";
            // 
            // txtBoxReisiKirjeldus
            // 
            this.txtBoxReisiKirjeldus.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "reisi_kirjeldus", true));
            this.txtBoxReisiKirjeldus.Location = new System.Drawing.Point(127, 152);
            this.txtBoxReisiKirjeldus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtBoxReisiKirjeldus.Name = "txtBoxReisiKirjeldus";
            this.txtBoxReisiKirjeldus.Size = new System.Drawing.Size(174, 20);
            this.txtBoxReisiKirjeldus.TabIndex = 1;
            // 
            // lbReisiAlgus
            // 
            this.lbReisiAlgus.AutoSize = true;
            this.lbReisiAlgus.Location = new System.Drawing.Point(24, 194);
            this.lbReisiAlgus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbReisiAlgus.Name = "lbReisiAlgus";
            this.lbReisiAlgus.Size = new System.Drawing.Size(58, 13);
            this.lbReisiAlgus.TabIndex = 0;
            this.lbReisiAlgus.Text = "Reisi algus";
            // 
            // lbReisiLopp
            // 
            this.lbReisiLopp.AutoSize = true;
            this.lbReisiLopp.Location = new System.Drawing.Point(24, 238);
            this.lbReisiLopp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbReisiLopp.Name = "lbReisiLopp";
            this.lbReisiLopp.Size = new System.Drawing.Size(53, 13);
            this.lbReisiLopp.TabIndex = 0;
            this.lbReisiLopp.Text = "Reisi lõpp";
            // 
            // lbReisiPaevadeArv
            // 
            this.lbReisiPaevadeArv.AutoSize = true;
            this.lbReisiPaevadeArv.Location = new System.Drawing.Point(24, 277);
            this.lbReisiPaevadeArv.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbReisiPaevadeArv.Name = "lbReisiPaevadeArv";
            this.lbReisiPaevadeArv.Size = new System.Drawing.Size(90, 13);
            this.lbReisiPaevadeArv.TabIndex = 0;
            this.lbReisiPaevadeArv.Text = "Reisipäevade arv";
            // 
            // txtBoxReisipaevadArv
            // 
            this.txtBoxReisipaevadArv.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "reisi_päevade_arv", true));
            this.txtBoxReisipaevadArv.Location = new System.Drawing.Point(127, 277);
            this.txtBoxReisipaevadArv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtBoxReisipaevadArv.Name = "txtBoxReisipaevadArv";
            this.txtBoxReisipaevadArv.Size = new System.Drawing.Size(174, 20);
            this.txtBoxReisipaevadArv.TabIndex = 1;
            // 
            // lbOlek
            // 
            this.lbOlek.AutoSize = true;
            this.lbOlek.Location = new System.Drawing.Point(24, 323);
            this.lbOlek.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbOlek.Name = "lbOlek";
            this.lbOlek.Size = new System.Drawing.Size(70, 13);
            this.lbOlek.TabIndex = 0;
            this.lbOlek.Text = "Aruande olek";
            // 
            // comboBoxAruandeOlek
            // 
            this.comboBoxAruandeOlek.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.bindingSourceKuluAruanne, "olek", true));
            this.comboBoxAruandeOlek.FormattingEnabled = true;
            this.comboBoxAruandeOlek.Location = new System.Drawing.Point(127, 321);
            this.comboBoxAruandeOlek.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxAruandeOlek.Name = "comboBoxAruandeOlek";
            this.comboBoxAruandeOlek.Size = new System.Drawing.Size(174, 21);
            this.comboBoxAruandeOlek.TabIndex = 2;
            // 
            // btnSalvestaAruanne
            // 
            this.btnSalvestaAruanne.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvestaAruanne.Location = new System.Drawing.Point(155, 372);
            this.btnSalvestaAruanne.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSalvestaAruanne.Name = "btnSalvestaAruanne";
            this.btnSalvestaAruanne.Size = new System.Drawing.Size(124, 37);
            this.btnSalvestaAruanne.TabIndex = 5;
            this.btnSalvestaAruanne.Text = "&Salvesta aruanne";
            this.btnSalvestaAruanne.UseVisualStyleBackColor = true;
            this.btnSalvestaAruanne.Click += new System.EventHandler(this.btnSalvestaAruanne_Click);
            // 
            // dtpReisiAlgus
            // 
            this.dtpReisiAlgus.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "reisi_algus", true));
            this.dtpReisiAlgus.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceKuluAruanne, "reisi_algus", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.dtpReisiAlgus.Location = new System.Drawing.Point(127, 194);
            this.dtpReisiAlgus.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpReisiAlgus.Name = "dtpReisiAlgus";
            this.dtpReisiAlgus.Size = new System.Drawing.Size(174, 20);
            this.dtpReisiAlgus.TabIndex = 6;
            // 
            // dtpReisiLopp
            // 
            this.dtpReisiLopp.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "reisi_lõpp", true));
            this.dtpReisiLopp.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.bindingSourceKuluAruanne, "reisi_lõpp", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.dtpReisiLopp.Location = new System.Drawing.Point(127, 238);
            this.dtpReisiLopp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpReisiLopp.Name = "dtpReisiLopp";
            this.dtpReisiLopp.Size = new System.Drawing.Size(174, 20);
            this.dtpReisiLopp.TabIndex = 7;
            // 
            // lbOtseseJuhiIsikukood
            // 
            this.lbOtseseJuhiIsikukood.AutoSize = true;
            this.lbOtseseJuhiIsikukood.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "otsene_juht", true));
            this.lbOtseseJuhiIsikukood.Location = new System.Drawing.Point(124, 112);
            this.lbOtseseJuhiIsikukood.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbOtseseJuhiIsikukood.Name = "lbOtseseJuhiIsikukood";
            this.lbOtseseJuhiIsikukood.Size = new System.Drawing.Size(107, 13);
            this.lbOtseseJuhiIsikukood.TabIndex = 8;
            this.lbOtseseJuhiIsikukood.Text = "Otsese juhi isikukood";
            // 
            // lbKoostajaIsikukood
            // 
            this.lbKoostajaIsikukood.AutoSize = true;
            this.lbKoostajaIsikukood.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "koostaja", true));
            this.lbKoostajaIsikukood.Location = new System.Drawing.Point(124, 74);
            this.lbKoostajaIsikukood.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbKoostajaIsikukood.Name = "lbKoostajaIsikukood";
            this.lbKoostajaIsikukood.Size = new System.Drawing.Size(96, 13);
            this.lbKoostajaIsikukood.TabIndex = 9;
            this.lbKoostajaIsikukood.Text = "Koostaja isikukood";
            // 
            // datagridUueAruandeSisestamiseFormis
            // 
            this.datagridUueAruandeSisestamiseFormis.AutoGenerateColumns = false;
            this.datagridUueAruandeSisestamiseFormis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridUueAruandeSisestamiseFormis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.kuluaruanneDataGridViewTextBoxColumn,
            this.kululiikDataGridViewTextBoxColumn,
            this.teenusepakkujaDataGridViewTextBoxColumn,
            this.kogusDataGridViewTextBoxColumn,
            this.kogukuluDataGridViewTextBoxColumn,
            this.dokumendidDataGridViewImageColumn});
            this.datagridUueAruandeSisestamiseFormis.DataSource = this.kulukirjedBindingSource;
            this.datagridUueAruandeSisestamiseFormis.Location = new System.Drawing.Point(339, 49);
            this.datagridUueAruandeSisestamiseFormis.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.datagridUueAruandeSisestamiseFormis.Name = "datagridUueAruandeSisestamiseFormis";
            this.datagridUueAruandeSisestamiseFormis.RowTemplate.Height = 24;
            this.datagridUueAruandeSisestamiseFormis.Size = new System.Drawing.Size(710, 152);
            this.datagridUueAruandeSisestamiseFormis.TabIndex = 10;
            // 
            // btnUusKulukirje
            // 
            this.btnUusKulukirje.Location = new System.Drawing.Point(484, 19);
            this.btnUusKulukirje.Name = "btnUusKulukirje";
            this.btnUusKulukirje.Size = new System.Drawing.Size(99, 25);
            this.btnUusKulukirje.TabIndex = 11;
            this.btnUusKulukirje.Text = "&Uus";
            this.btnUusKulukirje.UseVisualStyleBackColor = true;
            this.btnUusKulukirje.Click += new System.EventHandler(this.btnUusKulukirje_Click);
            // 
            // lblKulukirjedTeks
            // 
            this.lblKulukirjedTeks.AutoSize = true;
            this.lblKulukirjedTeks.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblKulukirjedTeks.Location = new System.Drawing.Point(334, 13);
            this.lblKulukirjedTeks.Name = "lblKulukirjedTeks";
            this.lblKulukirjedTeks.Size = new System.Drawing.Size(127, 29);
            this.lblKulukirjedTeks.TabIndex = 12;
            this.lblKulukirjedTeks.Text = "Kulukirjed:";
            // 
            // btnMuudaKulukirje
            // 
            this.btnMuudaKulukirje.Location = new System.Drawing.Point(608, 19);
            this.btnMuudaKulukirje.Name = "btnMuudaKulukirje";
            this.btnMuudaKulukirje.Size = new System.Drawing.Size(99, 25);
            this.btnMuudaKulukirje.TabIndex = 11;
            this.btnMuudaKulukirje.Text = "&Muuda";
            this.btnMuudaKulukirje.UseVisualStyleBackColor = true;
            // 
            // btnKustutaKulukirje
            // 
            this.btnKustutaKulukirje.Location = new System.Drawing.Point(734, 19);
            this.btnKustutaKulukirje.Name = "btnKustutaKulukirje";
            this.btnKustutaKulukirje.Size = new System.Drawing.Size(99, 25);
            this.btnKustutaKulukirje.TabIndex = 11;
            this.btnKustutaKulukirje.Text = "&Kustuta";
            this.btnKustutaKulukirje.UseVisualStyleBackColor = true;
            // 
            // lblAruandeId
            // 
            this.lblAruandeId.AutoSize = true;
            this.lblAruandeId.Location = new System.Drawing.Point(24, 29);
            this.lblAruandeId.Name = "lblAruandeId";
            this.lblAruandeId.Size = new System.Drawing.Size(81, 13);
            this.lblAruandeId.TabIndex = 13;
            this.lblAruandeId.Text = "Kuluaruande ID";
            // 
            // lblKuluaruandeIDNumber
            // 
            this.lblKuluaruandeIDNumber.AutoSize = true;
            this.lblKuluaruandeIDNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceKuluAruanne, "Id", true));
            this.lblKuluaruandeIDNumber.Location = new System.Drawing.Point(124, 29);
            this.lblKuluaruandeIDNumber.Name = "lblKuluaruandeIDNumber";
            this.lblKuluaruandeIDNumber.Size = new System.Drawing.Size(127, 13);
            this.lblKuluaruandeIDNumber.TabIndex = 14;
            this.lblKuluaruandeIDNumber.Text = "Siin on kuluaruande ID nr";
            // 
            // bindingSourceKuluAruanne
            // 
            this.bindingSourceKuluAruanne.DataSource = typeof(LogingBox.KuluAruanne);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // kuluaruanneDataGridViewTextBoxColumn
            // 
            this.kuluaruanneDataGridViewTextBoxColumn.DataPropertyName = "kuluaruanne";
            this.kuluaruanneDataGridViewTextBoxColumn.HeaderText = "kuluaruanne";
            this.kuluaruanneDataGridViewTextBoxColumn.Name = "kuluaruanneDataGridViewTextBoxColumn";
            this.kuluaruanneDataGridViewTextBoxColumn.Width = 50;
            // 
            // kululiikDataGridViewTextBoxColumn
            // 
            this.kululiikDataGridViewTextBoxColumn.DataPropertyName = "kululiik";
            this.kululiikDataGridViewTextBoxColumn.DataSource = this.kululiigidBindingSource;
            this.kululiikDataGridViewTextBoxColumn.DisplayMember = "liik";
            this.kululiikDataGridViewTextBoxColumn.HeaderText = "kululiik";
            this.kululiikDataGridViewTextBoxColumn.Name = "kululiikDataGridViewTextBoxColumn";
            this.kululiikDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.kululiikDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.kululiikDataGridViewTextBoxColumn.ValueMember = "Id";
            // 
            // kululiigidBindingSource
            // 
            this.kululiigidBindingSource.DataSource = typeof(LogingBox.Kululiigid);
            // 
            // teenusepakkujaDataGridViewTextBoxColumn
            // 
            this.teenusepakkujaDataGridViewTextBoxColumn.DataPropertyName = "teenusepakkuja";
            this.teenusepakkujaDataGridViewTextBoxColumn.HeaderText = "teenusepakkuja";
            this.teenusepakkujaDataGridViewTextBoxColumn.Name = "teenusepakkujaDataGridViewTextBoxColumn";
            this.teenusepakkujaDataGridViewTextBoxColumn.Width = 200;
            // 
            // kogusDataGridViewTextBoxColumn
            // 
            this.kogusDataGridViewTextBoxColumn.DataPropertyName = "kogus";
            this.kogusDataGridViewTextBoxColumn.HeaderText = "kogus";
            this.kogusDataGridViewTextBoxColumn.Name = "kogusDataGridViewTextBoxColumn";
            this.kogusDataGridViewTextBoxColumn.Width = 50;
            // 
            // kogukuluDataGridViewTextBoxColumn
            // 
            this.kogukuluDataGridViewTextBoxColumn.DataPropertyName = "kogukulu";
            this.kogukuluDataGridViewTextBoxColumn.HeaderText = "kogukulu";
            this.kogukuluDataGridViewTextBoxColumn.Name = "kogukuluDataGridViewTextBoxColumn";
            // 
            // dokumendidDataGridViewImageColumn
            // 
            this.dokumendidDataGridViewImageColumn.DataPropertyName = "dokumendid";
            this.dokumendidDataGridViewImageColumn.HeaderText = "dokumendid";
            this.dokumendidDataGridViewImageColumn.Name = "dokumendidDataGridViewImageColumn";
            // 
            // kulukirjedBindingSource
            // 
            this.kulukirjedBindingSource.DataSource = typeof(LogingBox.Kulukirjed);
            // 
            // UueSisestamine
            // 
            this.AcceptButton = this.btnSalvestaAruanne;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 435);
            this.Controls.Add(this.lblKuluaruandeIDNumber);
            this.Controls.Add(this.lblAruandeId);
            this.Controls.Add(this.lblKulukirjedTeks);
            this.Controls.Add(this.btnKustutaKulukirje);
            this.Controls.Add(this.btnMuudaKulukirje);
            this.Controls.Add(this.btnUusKulukirje);
            this.Controls.Add(this.datagridUueAruandeSisestamiseFormis);
            this.Controls.Add(this.lbKoostajaIsikukood);
            this.Controls.Add(this.lbOtseseJuhiIsikukood);
            this.Controls.Add(this.dtpReisiLopp);
            this.Controls.Add(this.dtpReisiAlgus);
            this.Controls.Add(this.btnSalvestaAruanne);
            this.Controls.Add(this.comboBoxAruandeOlek);
            this.Controls.Add(this.lbOlek);
            this.Controls.Add(this.txtBoxReisipaevadArv);
            this.Controls.Add(this.lbReisiPaevadeArv);
            this.Controls.Add(this.lbReisiLopp);
            this.Controls.Add(this.lbReisiAlgus);
            this.Controls.Add(this.txtBoxReisiKirjeldus);
            this.Controls.Add(this.lbReisiKirjeldus);
            this.Controls.Add(this.lbOtseneYlemus);
            this.Controls.Add(this.lbKoostaja);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "UueSisestamine";
            this.Text = "Uue aruande sisestamine";
            this.Load += new System.EventHandler(this.UueSisestamine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.datagridUueAruandeSisestamiseFormis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceKuluAruanne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kululiigidBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kulukirjedBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbKoostaja;
        private System.Windows.Forms.Label lbOtseneYlemus;
        private System.Windows.Forms.Label lbReisiKirjeldus;
        private System.Windows.Forms.TextBox txtBoxReisiKirjeldus;
        private System.Windows.Forms.Label lbReisiAlgus;
        private System.Windows.Forms.Label lbReisiLopp;
        private System.Windows.Forms.Label lbReisiPaevadeArv;
        private System.Windows.Forms.TextBox txtBoxReisipaevadArv;
        private System.Windows.Forms.Label lbOlek;
        private System.Windows.Forms.ComboBox comboBoxAruandeOlek;
        private System.Windows.Forms.Button btnSalvestaAruanne;
        private System.Windows.Forms.BindingSource bindingSourceKuluAruanne;
        private System.Windows.Forms.DateTimePicker dtpReisiAlgus;
        private System.Windows.Forms.DateTimePicker dtpReisiLopp;
        private System.Windows.Forms.Label lbOtseseJuhiIsikukood;
        private System.Windows.Forms.Label lbKoostajaIsikukood;
        private System.Windows.Forms.DataGridView datagridUueAruandeSisestamiseFormis;
        private System.Windows.Forms.BindingSource kulukirjedBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kuluaruanneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn kululiikDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource kululiigidBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn teenusepakkujaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kogusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kogukuluDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn dokumendidDataGridViewImageColumn;
        private System.Windows.Forms.Button btnUusKulukirje;
        private System.Windows.Forms.Label lblKulukirjedTeks;
        private System.Windows.Forms.Button btnMuudaKulukirje;
        private System.Windows.Forms.Button btnKustutaKulukirje;
        private System.Windows.Forms.Label lblAruandeId;
        private System.Windows.Forms.Label lblKuluaruandeIDNumber;
    }
}