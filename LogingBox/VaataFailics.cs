﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogingBox
{
    public partial class VaataFailics : Form
    {
        public Image ImageToShow { get; set; }
        public VaataFailics()
        {
            InitializeComponent();
        }

        private void VaataFailics_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = ImageToShow;
        }
    }
}
