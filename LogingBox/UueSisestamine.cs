﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogingBox
{
    public partial class UueSisestamine : Form
    {
        public UueSisestamine(KuluAruanne obj)
        {
            InitializeComponent();
            bindingSourceKuluAruanne.DataSource = obj;
            //kulukirjedBindingSource.DataSource = Program.rhs.Kulukirjed.ToList();
        }

        public KuluAruanne KuluAruandeInfo { get { return bindingSourceKuluAruanne.Current as KuluAruanne; } }

        private void UueSisestamine_Load(object sender, EventArgs e)
        {
            comboBoxAruandeOlek.DisplayMember = "Olek";
            comboBoxAruandeOlek.ValueMember = "Id";
            int kuluAruandeId = int.Parse(lblKuluaruandeIDNumber.Text);
            if (kuluAruandeId != 0) this.Text = "Kuluaruande muutmine";
            comboBoxAruandeOlek.DataSource = Program.rhs.AruandeOlek.ToList();
            kululiigidBindingSource.DataSource = Program.rhs.Kululiigid.ToList();
            datagridUueAruandeSisestamiseFormis.DataSource = Program.rhs.Kulukirjed.Where(x => x.KuluAruanne.Id == kuluAruandeId).ToList();
        }

        private void btnSalvestaAruanne_Click(object sender, EventArgs e)
        {
            if (!lbKoostajaIsikukood.Text.Equals(Program.kasutaja.Isikukood))
            {
                lbKoostajaIsikukood.Text = Program.kasutaja.Isikukood;
                lbOtseseJuhiIsikukood.Text = Program.kasutaja.OtseneÜlemus;
            }

            bindingSourceKuluAruanne.EndEdit(); 
            DialogResult = DialogResult.OK;
        }

        private void btnUusKulukirje_Click(object sender, EventArgs e)
        {
            UusKulukirje ukk = new UusKulukirje();
            ukk.ShowDialog();
        }
    }
}
