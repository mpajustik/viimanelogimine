﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;


namespace LogingBox
{
    public partial class AndmeteSisestus : Form
    {
        bool IsikukoodOlemas;
        Töötaja t;
        string Ylemus = "";
        string tekstMessageBoxis = null;

        public AndmeteSisestus()
        {
            InitializeComponent();

            PopuleeriDataGridJaYlemusBox();
            UuendaTextBoxid();
        }

        public void PopuleeriDataGridJaYlemusBox()
        {
            //dataGridView1.DataSource = Program.rhs.Töötaja.ToList();

            //foreach (DataGridViewColumn c in dataGridView1.Columns)
            //{
            //    c.Visible = false;
            //    switch (c.HeaderText)
            //    {
            //        case "Isikukood":
            //        case "AmetJaNimi":
            //        case "OtseneÜlemus":
            //        case "OnAktiivne":
            //        case "OnRaamatupidaja":
            //        case "OnAdmin":
            //            c.Visible = true;
            //            break;
            //    }
            //}

            // püüan datagridi mitte siduda datasource'iga
            dataGridView1.Columns.Clear();
            dataGridView1.ColumnCount = 7;
            dataGridView1.ColumnHeadersVisible = true;
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle() { Alignment = DataGridViewContentAlignment.MiddleCenter };
            dataGridView1.ColumnHeadersDefaultCellStyle = columnHeaderStyle;
            dataGridView1.DefaultCellStyle = columnHeaderStyle;
            dataGridView1.Columns[0].Name = "Isikukood";
            dataGridView1.Columns[1].Name = "Ametikoht";
            dataGridView1.Columns[2].Name = "Nimi";
            dataGridView1.Columns[3].Name = "Ülemus";
            dataGridView1.Columns[4].Name = "Rmtp";
            dataGridView1.Columns[5].Name = "Aktiivne";
            dataGridView1.Columns[6].Name = "Admin";

            foreach (var t in Program.rhs.Töötaja.ToList())
            {
                string[] rida = new string[]
                    {
                        t.Isikukood,
                        t.Ametikoht.Trim(),
                        t.Eesnimi+' '+t.Perekonnanimi,
                        t.Ülemus?.AmetJaNimi,
                        t.OnRaamatupidaja ? "Jah" : "-",
                        t.OnAktiivne ? "Jah" : "-",
                        t.OnAdmin ? "Jah" : "-"
                    };
                dataGridView1.Rows.Add(rida);
            }

            dataGridView1.Refresh();

            var dict = Program.rhs.Töötaja.ToDictionary(x => x.AmetJaNimi, x => x.Isikukood);
            dict.Add("(none)", "");
            YlemusBox1.DataSource = new BindingSource(dict, null);
            YlemusBox1.DisplayMember = "Key";
            YlemusBox1.ValueMember = "Value";
        }

        void UuendaTextBoxid()
        {
            EesnimiBox.Clear();
            PerekonnanimiBox.Clear();
            AmetikohtBox.Clear();
            YlemusBox1.SelectedValue = "" ;
            Ylemus = YlemusBox1.SelectedValue.ToString();
            RaamatupidajaCB1.Checked = false;
            AktiivneCB.Checked = false;
            IsikukoodiAbiLabel.Visible = false;
            SalvestanNupp.Text = "Loo uus kasutaja";
        }

        private void EesnimiLabel_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void AmetikohtLabel_Click(object sender, EventArgs e)
        {

        }

        private void SalvestanButton_Click(object sender, EventArgs e)
        {

            while (IsikukoodBox.Text !="")
            {
                if (IsikukoodOlemas)
                {
                    t.Eesnimi = EesnimiBox.Text;
                    t.Perekonnanimi = PerekonnanimiBox.Text;
                    t.Ametikoht = AmetikohtBox.Text;
                    if (Ylemus == "") t.OtseneÜlemus = null;
                    else t.OtseneÜlemus = Ylemus;
                    t.OnRaamatupidaja = RaamatupidajaCB1.Checked;
                    t.OnAktiivne = AktiivneCB.Checked;
                }
                else
                {
                    t = new Töötaja()
                    {
                        Isikukood = IsikukoodBox.Text,
                        Eesnimi = EesnimiBox.Text,
                        Perekonnanimi = PerekonnanimiBox.Text,
                        Ametikoht = AmetikohtBox.Text,
                        OtseneÜlemus = ((Ylemus == "") ? null : Ylemus),
                        OnRaamatupidaja = RaamatupidajaCB1.Checked,
                        OnAktiivne = AktiivneCB.Checked,
                        Salas6na = "0000"
                    };
                    Program.rhs.Töötaja.Add(t);
                }

                Program.rhs.SaveChanges();
                PopuleeriDataGridJaYlemusBox();

                if (IsikukoodOlemas) tekstMessageBoxis = $"'{t.AmetJaNimi}' andmed muudetud!!!";
                else tekstMessageBoxis = $"'{t.AmetJaNimi}' andmed salvestatud!!!";

                IsikukoodBox.Clear();
                UuendaTextBoxid();
                MessageBox.Show(tekstMessageBoxis);
            }
        }

        private void True(object sender, EventArgs e)
        {

        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

        private bool KontrolliIsikukood(string isikukood)
        {
            if (isikukood.Length != 11) return false;

            int i = 0;

            int kj =
            isikukood.Substring(0, 10)
                .Select(x => x - '0')
                .ToArray()
                .Select(x => x * ((i++ % 9) + 1))
                .Sum()
                % 11
                ;
            if (kj == 10)
            {
                i = 2;
                kj = isikukood.Substring(0, 10).ToArray()
                    .Select(x => x * ((i++ % 9) + 1))
                .Sum() % 11 % 10;
            }

            return kj == isikukood[10] - '0';
        }

        private void IsikukoodBox_TextChanged(object sender, EventArgs e)
        {
            if (!KontrolliIsikukood(IsikukoodBox.Text))
            {
                IsikukoodiAbiLabel.Text = "Isikukood vigane";
                IsikukoodiAbiLabel.Visible = true;
            }
            else
            {
                IsikukoodiAbiLabel.Visible = false;
                IsikukoodiAbiLabel.Text = "See isikukood on juba olemas";

                IsikukoodOlemas = Program.rhs.Töötaja.Where(x => x.Isikukood == IsikukoodBox.Text).Count() > 0;

                if (IsikukoodOlemas)
                {
                    t = Program.rhs.Töötaja.Where(x => x.Isikukood == IsikukoodBox.Text).Single();

                    IsikukoodiAbiLabel.Visible = true;
                    SalvestanNupp.Text = "Muuda kasutaja andmeid";
                    EesnimiBox.Text = t.Eesnimi.Trim();
                    PerekonnanimiBox.Text = t.Perekonnanimi.Trim();
                    AmetikohtBox.Text = t.Ametikoht.Trim();
                    Ylemus = t.OtseneÜlemus ?? "";
                    YlemusBox1.SelectedValue = Ylemus ?? "";
                    RaamatupidajaCB1.Checked = t.OnRaamatupidaja;
                    AktiivneCB.Checked = t.OnAktiivne;
                }
                else
                {
                    IsikukoodiAbiLabel.Visible = false;
                    SalvestanNupp.Text = "Loo uus kasutaja";
                    UuendaTextBoxid();
                }
            }
        }

        private void EesnimiBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void KontrollBox_Click(object sender, EventArgs e)
        {
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (YlemusBox1.Focused) Ylemus = YlemusBox1.SelectedValue.ToString();
        }

        private void DataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            IsikukoodBox.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            //IsikukoodBox.Text = ((Töötaja)dataGridView1.CurrentRow.DataBoundItem).Isikukood;
        }

        private void KustutamiseButton_Click(object sender, EventArgs e)
        {
            string valitudIsikukood = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            t = Program.rhs.Töötaja.Where(x => x.Isikukood == valitudIsikukood).Single();
            //t = Program.rhs.Töötaja.Where(x => x.Isikukood == ((Töötaja)dataGridView1.CurrentRow.DataBoundItem).Isikukood).Single();
            if (MessageBox.Show($"Kas oled kindel, et soovid kustutada '{t.AmetJaNimi}'?", "Kas oled kindel?",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Program.rhs.Töötaja.Remove(t);
                Program.rhs.SaveChanges();
                PopuleeriDataGridJaYlemusBox();
                UuendaTextBoxid();
                tekstMessageBoxis = $"Isik '{t.AmetJaNimi}' kustutatud!!!";
                MessageBox.Show(tekstMessageBoxis);
            }
        }

        private void AndmeteSisestus_Load(object sender, EventArgs e)
        {

        }

      
    }
}
