﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace LogingBox
{
    class Logimine_kood
    {

    }

    public partial class ReisikuludeHaldamiseSysteemVMEntities1
    {
        public override int SaveChanges()
        {
            var muudetudAndmed = ChangeTracker.Entries()
                .Where(p => p.State != EntityState.Unchanged).ToList();

            var praegu = DateTime.UtcNow;

            foreach (var muudatus in muudetudAndmed)
            {
                string muudetudAsi;
                IEnumerable<string> muudetudVäljad = null;

                var primaryKey = GetPrimaryKeyValue(muudatus);

                if (muudatus.State == EntityState.Added)
                {
                    muudetudAsi = muudatus.CurrentValues.ToObject().GetType().Name.Substring(0, 20);
                    muudetudVäljad = muudatus.CurrentValues.PropertyNames;

                    foreach (var prop in muudetudVäljad)
                    {
                        string originalValue = "";
                        string currentValue = muudatus?.CurrentValues[prop]?.ToString();

                        ChangeLog log = new ChangeLog()
                        {
                            midaMuudeti = muudetudAsi,
                            instantsiId = primaryKey.ToString(),
                            propertyNimi = prop,
                            vanaVäärtus = originalValue,
                            uusVäärtus = currentValue,
                            muutmiseAegUTC = praegu,
                            kesMuutis = Program.kasutaja.Isikukood
                        };
                        Program.rhs.ChangeLog.Add(log);
                    }
                }
                else
                {
                    muudetudAsi = muudatus.Entity.GetType().Name.Substring(0, 20);
                    muudetudVäljad = muudatus.OriginalValues.PropertyNames;

                    foreach (var prop in muudetudVäljad)
                    {
                        string originalValue = muudatus.OriginalValues[prop]?.ToString();
                        string currentValue;
                        if (muudatus.State == EntityState.Deleted) { currentValue = null; }
                        else { currentValue = muudatus?.CurrentValues[prop]?.ToString(); }

                        if (originalValue?.Trim() != currentValue?.Trim())
                        {
                            ChangeLog log = new ChangeLog()
                            {
                                midaMuudeti = muudetudAsi,
                                instantsiId = primaryKey.ToString(),
                                propertyNimi = prop,
                                vanaVäärtus = originalValue,
                                uusVäärtus = currentValue,
                                muutmiseAegUTC = praegu,
                                kesMuutis = Program.kasutaja.Isikukood
                            };
                            Program.rhs.ChangeLog.Add(log);
                        }
                    }
                }
            }
            return base.SaveChanges();
        }
        object GetPrimaryKeyValue(DbEntityEntry entry)
        {
            var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            if (objectStateEntry.State == EntityState.Added)
                { return "uus" ; }
            else
                { return objectStateEntry.EntityKey.EntityKeyValues[0].Value; }
        }
    }


}
