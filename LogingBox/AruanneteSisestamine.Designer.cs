﻿namespace LogingBox
{
    partial class AruanneteSisestamine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridAruanneteSisestamiseKast = new System.Windows.Forms.DataGridView();
            this.btnUus = new System.Windows.Forms.Button();
            this.btnMuuda = new System.Windows.Forms.Button();
            this.btnKustuta = new System.Windows.Forms.Button();
            this.btnEdasta = new System.Windows.Forms.Button();
            this.dataGridAruannetKastisKulukirjed = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kuluaruanneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kululiikDataGridViewComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.kululiigidBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teenusepakkujaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kogusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kogukuluDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dokumendidDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.kulukirjedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.koostajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.otsenejuhtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reisikirjeldusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reisialgusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reisilõppDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reisipäevadearvDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.olekDataGridViewComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aruandeOlekBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.kuluAruanneBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAruanneteSisestamiseKast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAruannetKastisKulukirjed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kululiigidBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kulukirjedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aruandeOlekBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kuluAruanneBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridAruanneteSisestamiseKast
            // 
            this.dataGridAruanneteSisestamiseKast.AutoGenerateColumns = false;
            this.dataGridAruanneteSisestamiseKast.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAruanneteSisestamiseKast.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.koostajaDataGridViewTextBoxColumn,
            this.otsenejuhtDataGridViewTextBoxColumn,
            this.reisikirjeldusDataGridViewTextBoxColumn,
            this.reisialgusDataGridViewTextBoxColumn,
            this.reisilõppDataGridViewTextBoxColumn,
            this.reisipäevadearvDataGridViewTextBoxColumn,
            this.olekDataGridViewComboBox});
            this.dataGridAruanneteSisestamiseKast.DataSource = this.kuluAruanneBindingSource;
            this.dataGridAruanneteSisestamiseKast.Location = new System.Drawing.Point(12, 98);
            this.dataGridAruanneteSisestamiseKast.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridAruanneteSisestamiseKast.Name = "dataGridAruanneteSisestamiseKast";
            this.dataGridAruanneteSisestamiseKast.RowTemplate.Height = 24;
            this.dataGridAruanneteSisestamiseKast.Size = new System.Drawing.Size(1295, 350);
            this.dataGridAruanneteSisestamiseKast.TabIndex = 0;
            this.dataGridAruanneteSisestamiseKast.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridAruanneteSisestamiseKast_CellContentClick);
            // 
            // btnUus
            // 
            this.btnUus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUus.Location = new System.Drawing.Point(51, 46);
            this.btnUus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUus.Name = "btnUus";
            this.btnUus.Size = new System.Drawing.Size(108, 33);
            this.btnUus.TabIndex = 1;
            this.btnUus.Text = "&Uus";
            this.btnUus.UseVisualStyleBackColor = true;
            this.btnUus.Click += new System.EventHandler(this.btnUus_Click);
            // 
            // btnMuuda
            // 
            this.btnMuuda.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMuuda.Location = new System.Drawing.Point(180, 46);
            this.btnMuuda.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMuuda.Name = "btnMuuda";
            this.btnMuuda.Size = new System.Drawing.Size(108, 33);
            this.btnMuuda.TabIndex = 1;
            this.btnMuuda.Text = "&Muuda";
            this.btnMuuda.UseVisualStyleBackColor = true;
            this.btnMuuda.Click += new System.EventHandler(this.btnMuuda_Click);
            // 
            // btnKustuta
            // 
            this.btnKustuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKustuta.Location = new System.Drawing.Point(309, 46);
            this.btnKustuta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnKustuta.Name = "btnKustuta";
            this.btnKustuta.Size = new System.Drawing.Size(108, 33);
            this.btnKustuta.TabIndex = 1;
            this.btnKustuta.Text = "&Kustuta";
            this.btnKustuta.UseVisualStyleBackColor = true;
            this.btnKustuta.Click += new System.EventHandler(this.btnKustuta_Click);
            // 
            // btnEdasta
            // 
            this.btnEdasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdasta.Location = new System.Drawing.Point(445, 46);
            this.btnEdasta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEdasta.Name = "btnEdasta";
            this.btnEdasta.Size = new System.Drawing.Size(108, 33);
            this.btnEdasta.TabIndex = 1;
            this.btnEdasta.Text = "&Edasta";
            this.btnEdasta.UseVisualStyleBackColor = true;
            // 
            // dataGridAruannetKastisKulukirjed
            // 
            this.dataGridAruannetKastisKulukirjed.AutoGenerateColumns = false;
            this.dataGridAruannetKastisKulukirjed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAruannetKastisKulukirjed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.kuluaruanneDataGridViewTextBoxColumn,
            this.kululiikDataGridViewComboBox,
            this.teenusepakkujaDataGridViewTextBoxColumn,
            this.kogusDataGridViewTextBoxColumn,
            this.kogukuluDataGridViewTextBoxColumn,
            this.dokumendidDataGridViewImageColumn});
            this.dataGridAruannetKastisKulukirjed.DataSource = this.kulukirjedBindingSource;
            this.dataGridAruannetKastisKulukirjed.Location = new System.Drawing.Point(17, 506);
            this.dataGridAruannetKastisKulukirjed.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridAruannetKastisKulukirjed.Name = "dataGridAruannetKastisKulukirjed";
            this.dataGridAruannetKastisKulukirjed.Size = new System.Drawing.Size(1237, 315);
            this.dataGridAruannetKastisKulukirjed.TabIndex = 2;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            // 
            // kuluaruanneDataGridViewTextBoxColumn
            // 
            this.kuluaruanneDataGridViewTextBoxColumn.DataPropertyName = "kuluaruanne";
            this.kuluaruanneDataGridViewTextBoxColumn.HeaderText = "kuluaruanne";
            this.kuluaruanneDataGridViewTextBoxColumn.Name = "kuluaruanneDataGridViewTextBoxColumn";
            // 
            // kululiikDataGridViewComboBox
            // 
            this.kululiikDataGridViewComboBox.DataPropertyName = "kululiik";
            this.kululiikDataGridViewComboBox.DataSource = this.kululiigidBindingSource;
            this.kululiikDataGridViewComboBox.DisplayMember = "liik";
            this.kululiikDataGridViewComboBox.HeaderText = "kululiik";
            this.kululiikDataGridViewComboBox.Name = "kululiikDataGridViewComboBox";
            this.kululiikDataGridViewComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.kululiikDataGridViewComboBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.kululiikDataGridViewComboBox.ValueMember = "Id";
            // 
            // kululiigidBindingSource
            // 
            this.kululiigidBindingSource.DataSource = typeof(LogingBox.Kululiigid);
            // 
            // teenusepakkujaDataGridViewTextBoxColumn
            // 
            this.teenusepakkujaDataGridViewTextBoxColumn.DataPropertyName = "teenusepakkuja";
            this.teenusepakkujaDataGridViewTextBoxColumn.HeaderText = "teenusepakkuja";
            this.teenusepakkujaDataGridViewTextBoxColumn.Name = "teenusepakkujaDataGridViewTextBoxColumn";
            this.teenusepakkujaDataGridViewTextBoxColumn.Width = 200;
            // 
            // kogusDataGridViewTextBoxColumn
            // 
            this.kogusDataGridViewTextBoxColumn.DataPropertyName = "kogus";
            this.kogusDataGridViewTextBoxColumn.HeaderText = "kogus";
            this.kogusDataGridViewTextBoxColumn.Name = "kogusDataGridViewTextBoxColumn";
            // 
            // kogukuluDataGridViewTextBoxColumn
            // 
            this.kogukuluDataGridViewTextBoxColumn.DataPropertyName = "kogukulu";
            this.kogukuluDataGridViewTextBoxColumn.HeaderText = "kogukulu";
            this.kogukuluDataGridViewTextBoxColumn.Name = "kogukuluDataGridViewTextBoxColumn";
            // 
            // dokumendidDataGridViewImageColumn
            // 
            this.dokumendidDataGridViewImageColumn.DataPropertyName = "dokumendid";
            this.dokumendidDataGridViewImageColumn.HeaderText = "dokumendid";
            this.dokumendidDataGridViewImageColumn.Name = "dokumendidDataGridViewImageColumn";
            // 
            // kulukirjedBindingSource
            // 
            this.kulukirjedBindingSource.DataSource = typeof(LogingBox.Kulukirjed);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // koostajaDataGridViewTextBoxColumn
            // 
            this.koostajaDataGridViewTextBoxColumn.DataPropertyName = "koostaja";
            this.koostajaDataGridViewTextBoxColumn.HeaderText = "koostaja";
            this.koostajaDataGridViewTextBoxColumn.Name = "koostajaDataGridViewTextBoxColumn";
            // 
            // otsenejuhtDataGridViewTextBoxColumn
            // 
            this.otsenejuhtDataGridViewTextBoxColumn.DataPropertyName = "otsene_juht";
            this.otsenejuhtDataGridViewTextBoxColumn.HeaderText = "Otsene juht";
            this.otsenejuhtDataGridViewTextBoxColumn.Name = "otsenejuhtDataGridViewTextBoxColumn";
            // 
            // reisikirjeldusDataGridViewTextBoxColumn
            // 
            this.reisikirjeldusDataGridViewTextBoxColumn.DataPropertyName = "reisi_kirjeldus";
            this.reisikirjeldusDataGridViewTextBoxColumn.HeaderText = "Reisi kirjeldus";
            this.reisikirjeldusDataGridViewTextBoxColumn.Name = "reisikirjeldusDataGridViewTextBoxColumn";
            this.reisikirjeldusDataGridViewTextBoxColumn.Width = 150;
            // 
            // reisialgusDataGridViewTextBoxColumn
            // 
            this.reisialgusDataGridViewTextBoxColumn.DataPropertyName = "reisi_algus";
            this.reisialgusDataGridViewTextBoxColumn.HeaderText = "Reisi algus";
            this.reisialgusDataGridViewTextBoxColumn.Name = "reisialgusDataGridViewTextBoxColumn";
            // 
            // reisilõppDataGridViewTextBoxColumn
            // 
            this.reisilõppDataGridViewTextBoxColumn.DataPropertyName = "reisi_lõpp";
            this.reisilõppDataGridViewTextBoxColumn.HeaderText = "Reisi lõpp";
            this.reisilõppDataGridViewTextBoxColumn.Name = "reisilõppDataGridViewTextBoxColumn";
            // 
            // reisipäevadearvDataGridViewTextBoxColumn
            // 
            this.reisipäevadearvDataGridViewTextBoxColumn.DataPropertyName = "reisi_päevade_arv";
            this.reisipäevadearvDataGridViewTextBoxColumn.HeaderText = "Reisipäevade arv";
            this.reisipäevadearvDataGridViewTextBoxColumn.Name = "reisipäevadearvDataGridViewTextBoxColumn";
            // 
            // olekDataGridViewComboBox
            // 
            this.olekDataGridViewComboBox.DataPropertyName = "olek";
            this.olekDataGridViewComboBox.DataSource = this.aruandeOlekBindingSource;
            this.olekDataGridViewComboBox.DisplayMember = "Olek";
            this.olekDataGridViewComboBox.HeaderText = "Aruande olek";
            this.olekDataGridViewComboBox.Name = "olekDataGridViewComboBox";
            this.olekDataGridViewComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.olekDataGridViewComboBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.olekDataGridViewComboBox.ValueMember = "Id";
            // 
            // aruandeOlekBindingSource
            // 
            this.aruandeOlekBindingSource.DataSource = typeof(LogingBox.AruandeOlek);
            // 
            // kuluAruanneBindingSource
            // 
            this.kuluAruanneBindingSource.DataSource = typeof(LogingBox.KuluAruanne);
            // 
            // AruanneteSisestamine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1319, 896);
            this.Controls.Add(this.dataGridAruannetKastisKulukirjed);
            this.Controls.Add(this.btnEdasta);
            this.Controls.Add(this.btnKustuta);
            this.Controls.Add(this.btnMuuda);
            this.Controls.Add(this.btnUus);
            this.Controls.Add(this.dataGridAruanneteSisestamiseKast);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AruanneteSisestamine";
            this.Text = "AruanneteSisestamine";
            this.Load += new System.EventHandler(this.AruanneteSisestamine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAruanneteSisestamiseKast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAruannetKastisKulukirjed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kululiigidBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kulukirjedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aruandeOlekBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kuluAruanneBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridAruanneteSisestamiseKast;
        private System.Windows.Forms.Button btnUus;
        private System.Windows.Forms.Button btnMuuda;
        private System.Windows.Forms.Button btnKustuta;
        private System.Windows.Forms.Button btnEdasta;
        private System.Windows.Forms.BindingSource kuluAruanneBindingSource;
        private System.Windows.Forms.BindingSource aruandeOlekBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn koostajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn otsenejuhtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reisikirjeldusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reisialgusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reisilõppDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reisipäevadearvDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn olekDataGridViewComboBox;
        private System.Windows.Forms.DataGridView dataGridAruannetKastisKulukirjed;
        private System.Windows.Forms.BindingSource kulukirjedBindingSource;
        private System.Windows.Forms.BindingSource kululiigidBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kuluaruanneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn kululiikDataGridViewComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn teenusepakkujaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kogusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kogukuluDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn dokumendidDataGridViewImageColumn;
    }
}