﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace LogingBox
{
    public partial class Logid : Form
    {
        Timer taimer = new Timer();

        public Logid()
        {
            InitializeComponent();

            ToolTip toolTip1 = new ToolTip();
            toolTip1.SetToolTip(FilterTühjaksButton1, "Puhasta kõik filtrid");
            toolTip1.SetToolTip(label1, "Filtreeri tulpade sisu järgi, kasvõi mitu filtrit korraga");

            JoonistaDataGrid();
        }

        DataTable dt = new DataTable();

        public void JoonistaDataGrid()
        {
            dt.Columns.Add("Id");
            dt.Columns.Add("mida muudeti");
            dt.Columns.Add("instantsi Id");
            dt.Columns.Add("property nimi");
            dt.Columns.Add("vana väärtus");
            dt.Columns.Add("uus väärtus");
            dt.Columns.Add("muutmise aeg UTC");
            dt.Columns.Add("kes muutis");

            foreach (var clr in Program.rhs.ChangeLog)
            {
                var dr = new string[]
                {
                    clr.Id.ToString("0000000000"),
                    clr.midaMuudeti,
                    clr.instantsiId,
                    clr.propertyNimi,
                    clr.vanaVäärtus,
                    clr.uusVäärtus,
                    clr.muutmiseAegUTC.ToString(),
                    clr.kesMuutis
                };
                dt.Rows.Add(dr);
            }

            dataGridView1.DataSource = dt;

            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle() { Alignment = DataGridViewContentAlignment.MiddleCenter };
            dataGridView1.ColumnHeadersDefaultCellStyle = columnHeaderStyle;
            dataGridView1.DefaultCellStyle = columnHeaderStyle;
            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Descending);

            foreach (DataGridViewColumn c in dataGridView1.Columns)
                c.ToolTipText = "Siin klõpsates saad sorteerida selle tulba järgi\n(vaheldumisi kasvavas või kahanevas järjekorras)";

            Label2.Text = "ridu: " + dt.DefaultView.Count.ToString();
        }

        private void FiltreeriChangeLogiDataGrid()
        {
            #region StringBuilder mitmese filtri väärtustamiseks

            StringBuilder sb = new StringBuilder();

            if (IdTextBox1.Text.Length > 0) sb.Append($"[Id] LIKE '%{IdTextBox1.Text}%'");

            if (KlassTextBox2.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[mida muudeti] LIKE '%{KlassTextBox2.Text}%'");
            }

            if (InstantsTextBox3.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[instantsi Id] LIKE '%{InstantsTextBox3.Text}%'");
            }

            if (PropertyTextBox4.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[property nimi] LIKE '%{PropertyTextBox4.Text}%'");
            }

            if (VanaTextBox5.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[vana väärtus] LIKE '%{VanaTextBox5.Text}%'");
            }

            if (UusTextBox6.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[uus väärtus] LIKE '%{UusTextBox6.Text}%'");
            }

            if (KunasTextBox7.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[muutmise aeg UTC] LIKE '%{KunasTextBox7.Text}%'");
            }

            if (KesTextBox8.Text.Length > 0)
            {
                if (sb.Length > 0) sb.Append(" AND ");
                sb.Append($"[kes muutis] LIKE '%{KesTextBox8.Text}%'");
            }

            #endregion

            dt.DefaultView.RowFilter = sb.ToString();
            Label2.Text = "ridu: " + dt.DefaultView.Count.ToString();
        }

        #region filtri tekstbokside TextChanged actionid triggerdavad kõik FiltreeriChangeLogiDataGrid()'i

        private void IdTextBox1_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void KlassTextBox2_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void InstantsTextBox3_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void PropertyTextBox4_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void VanaTextBox5_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void UusTextBox6_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void KunasTextBox7_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        private void KesTextBox8_TextChanged(object sender, EventArgs e)
        {
            FiltreeriChangeLogiDataGrid();
        }

        #endregion

        private void FilterTühjaksButton1_Click(object sender, EventArgs e)
        {
            foreach (Control c in Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (dataGridView1.CurrentCell.ColumnIndex)
            {
                case 0:
                    IdTextBox1.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 1:
                    KlassTextBox2.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 2:
                    InstantsTextBox3.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 3:
                    PropertyTextBox4.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 4:
                    VanaTextBox5.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 5:
                    UusTextBox6.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 6:
                    KunasTextBox7.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
                case 7:
                    KesTextBox8.Text = dataGridView1.CurrentCell.Value.ToString();
                    break;
            }
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            taimer.Interval = 2000;
            taimer.Start();
            taimer.Tick += new EventHandler(NäitaCellToolTipi);

            void NäitaCellToolTipi(object sender2, EventArgs e2)
            {
                toolTip1.Show("Topeltklõps väljal lisab filtrisse selle välja väärtuse",
                    this, dataGridView1.PointToClient(Cursor.Position),
                    5000);
            }
        }

        private void dataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            taimer.Stop();
            toolTip1.Hide(this);
        }
    }
    public partial class ReisikuludeHaldamiseSysteemVMEntities1
    {
        public override int SaveChanges()
        {
            var muudetudAndmed = ChangeTracker.Entries()
                .Where(p => p.State != EntityState.Unchanged).ToList();

            var praegu = DateTime.UtcNow;

            foreach (var muudatus in muudetudAndmed)
            {
                string muudetudAsi;
                int alakriipsuIndeks;
                IEnumerable<string> muudetudVäljad = null;

                var primaryKey = GetPrimaryKeyValue(muudatus);

                if (muudatus.State == EntityState.Added)
                {
                    muudetudAsi = muudatus.CurrentValues.ToObject().GetType().Name;
                    alakriipsuIndeks = muudetudAsi.IndexOf('_');
                    muudetudAsi = muudetudAsi.Substring(0, alakriipsuIndeks);
                    muudetudVäljad = muudatus.CurrentValues.PropertyNames;

                    foreach (var prop in muudetudVäljad)
                    {
                        string originalValue = "";
                        string currentValue = muudatus?.CurrentValues[prop]?.ToString();

                        ChangeLog log = new ChangeLog()
                        {
                            midaMuudeti = muudetudAsi,
                            instantsiId = primaryKey.ToString(),
                            propertyNimi = prop,
                            vanaVäärtus = originalValue,
                            uusVäärtus = currentValue,
                            muutmiseAegUTC = praegu,
                            kesMuutis = Program.kasutaja.Isikukood
                        };
                        Program.rhs.ChangeLog.Add(log);
                    }
                }
                else
                {
                    muudetudAsi = muudatus.Entity.GetType().Name;
                    alakriipsuIndeks = muudetudAsi.IndexOf('_');
                    muudetudAsi = muudetudAsi.Substring(0, alakriipsuIndeks);
                    muudetudVäljad = muudatus.OriginalValues.PropertyNames;

                    foreach (var prop in muudetudVäljad)
                    {
                        string originalValue = muudatus.OriginalValues[prop]?.ToString();
                        string currentValue;
                        if (muudatus.State == EntityState.Deleted) { currentValue = null; }
                        else { currentValue = muudatus?.CurrentValues[prop]?.ToString(); }

                        if (originalValue?.Trim() != currentValue?.Trim())
                        {
                            ChangeLog log = new ChangeLog()
                            {
                                midaMuudeti = muudetudAsi,
                                instantsiId = primaryKey.ToString(),
                                propertyNimi = prop,
                                vanaVäärtus = originalValue,
                                uusVäärtus = currentValue,
                                muutmiseAegUTC = praegu,
                                kesMuutis = Program.kasutaja.Isikukood
                            };
                            Program.rhs.ChangeLog.Add(log);
                        }
                    }
                }
            }
            return base.SaveChanges();
        }
        object GetPrimaryKeyValue(DbEntityEntry entry)
        {
            var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            if (objectStateEntry.State == EntityState.Added)
            { return "uus"; }
            else
            { return objectStateEntry.EntityKey.EntityKeyValues[0].Value; }
        }
    }
}
