﻿USE [ReisikuludeHaldamiseSysteemVM]
GO

DECLARE	@return_value Int

EXEC	@return_value = [dbo].[CheckIsik]
		@ik = N'123',
		@pw = N'0000'

SELECT	@return_value as 'Return Value'

GO
